# Generated from /home/henry/Workspace/chab/XPPLexer.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2+")
        buf.write("\u0174\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\3\2\3\2\3\3\3\3\5\3p\n\3\3\4\3\4\5\4t")
        buf.write("\n\4\3\5\3\5\5\5x\n\5\3\6\3\6\5\6|\n\6\3\6\7\6\177\n\6")
        buf.write("\f\6\16\6\u0082\13\6\3\7\3\7\5\7\u0086\n\7\3\7\7\7\u0089")
        buf.write("\n\7\f\7\16\7\u008c\13\7\3\b\3\b\5\b\u0090\n\b\3\b\7\b")
        buf.write("\u0093\n\b\f\b\16\b\u0096\13\b\3\t\3\t\5\t\u009a\n\t\3")
        buf.write("\t\7\t\u009d\n\t\f\t\16\t\u00a0\13\t\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\n\3\n\5\n\u00a9\n\n\3\n\5\n\u00ac\n\n\3\13\3\13\3")
        buf.write("\13\3\13\3\13\3\13\7\13\u00b4\n\13\f\13\16\13\u00b7\13")
        buf.write("\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\5\f\u00d5\n\f\3\r\6\r\u00d8\n\r\r\r\16\r\u00d9")
        buf.write("\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\26\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30")
        buf.write("\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3\"")
        buf.write("\3\"\3#\3#\3#\3#\3$\3$\3$\3$\3$\3%\3%\3&\3&\3\'\3\'\3")
        buf.write("\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3)\3*")
        buf.write("\3*\3*\3*\3*\3*\3+\3+\3+\3+\3+\3+\3+\3,\3,\3-\3-\3.\3")
        buf.write(".\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\62\3\62\3\63\3")
        buf.write("\63\5\63\u016a\n\63\3\64\3\64\3\65\3\65\7\65\u0170\n\65")
        buf.write("\f\65\16\65\u0173\13\65\2\2\66\3\2\5\2\7\2\t\2\13\2\r")
        buf.write("\2\17\2\21\2\23\3\25\4\27\2\31\5\33\6\35\7\37\b!\t#\n")
        buf.write("%\13\'\f)\r+\16-\17/\20\61\21\63\22\65\23\67\249\25;\26")
        buf.write("=\27?\30A\31C\32E\33G\34I\35K\36M\37O Q!S\"U#W$Y%[&]\'")
        buf.write("_(a)c*e\2g\2i+\3\2\13\4\2C\\c|\4\2DDdd\4\2ZZzz\4\2QQq")
        buf.write("q\4\2$$^^\n\2\62\62^^cdhhppttvvxx\5\2\13\f\17\17\"\"\7")
        buf.write("\2\62;aa\u00b9\u00b9\u0302\u0371\u2041\u2042\17\2C\\c")
        buf.write("|\u00c2\u00d8\u00da\u00f8\u00fa\u0301\u0372\u037f\u0381")
        buf.write("\u2001\u200e\u200f\u2072\u2191\u2c02\u2ff1\u3003\ud801")
        buf.write("\uf902\ufdd1\ufdf2\uffff\2\u0180\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2")
        buf.write("\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2")
        buf.write("\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63")
        buf.write("\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2")
        buf.write("\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2")
        buf.write("\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3")
        buf.write("\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y")
        buf.write("\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2")
        buf.write("c\3\2\2\2\2i\3\2\2\2\3k\3\2\2\2\5o\3\2\2\2\7s\3\2\2\2")
        buf.write("\tw\3\2\2\2\13y\3\2\2\2\r\u0083\3\2\2\2\17\u008d\3\2\2")
        buf.write("\2\21\u0097\3\2\2\2\23\u00ab\3\2\2\2\25\u00ad\3\2\2\2")
        buf.write("\27\u00d4\3\2\2\2\31\u00d7\3\2\2\2\33\u00dd\3\2\2\2\35")
        buf.write("\u00e0\3\2\2\2\37\u00e6\3\2\2\2!\u00ec\3\2\2\2#\u00ee")
        buf.write("\3\2\2\2%\u00fa\3\2\2\2\'\u00fc\3\2\2\2)\u0101\3\2\2\2")
        buf.write("+\u0103\3\2\2\2-\u0106\3\2\2\2/\u010e\3\2\2\2\61\u0112")
        buf.write("\3\2\2\2\63\u0114\3\2\2\2\65\u0117\3\2\2\2\67\u011a\3")
        buf.write("\2\2\29\u011e\3\2\2\2;\u0120\3\2\2\2=\u0122\3\2\2\2?\u0124")
        buf.write("\3\2\2\2A\u0127\3\2\2\2C\u0129\3\2\2\2E\u012b\3\2\2\2")
        buf.write("G\u012f\3\2\2\2I\u0134\3\2\2\2K\u0136\3\2\2\2M\u0138\3")
        buf.write("\2\2\2O\u013e\3\2\2\2Q\u0143\3\2\2\2S\u014a\3\2\2\2U\u0150")
        buf.write("\3\2\2\2W\u0157\3\2\2\2Y\u0159\3\2\2\2[\u015b\3\2\2\2")
        buf.write("]\u015d\3\2\2\2_\u015f\3\2\2\2a\u0161\3\2\2\2c\u0163\3")
        buf.write("\2\2\2e\u0169\3\2\2\2g\u016b\3\2\2\2i\u016d\3\2\2\2kl")
        buf.write("\4\62\63\2l\4\3\2\2\2mp\5\3\2\2np\4\649\2om\3\2\2\2on")
        buf.write("\3\2\2\2p\6\3\2\2\2qt\5\5\3\2rt\4:;\2sq\3\2\2\2sr\3\2")
        buf.write("\2\2t\b\3\2\2\2ux\5\7\4\2vx\t\2\2\2wu\3\2\2\2wv\3\2\2")
        buf.write("\2x\n\3\2\2\2y\u0080\5\3\2\2z|\7a\2\2{z\3\2\2\2{|\3\2")
        buf.write("\2\2|}\3\2\2\2}\177\5\3\2\2~{\3\2\2\2\177\u0082\3\2\2")
        buf.write("\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\f\3\2\2\2")
        buf.write("\u0082\u0080\3\2\2\2\u0083\u008a\5\5\3\2\u0084\u0086\7")
        buf.write("a\2\2\u0085\u0084\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087")
        buf.write("\3\2\2\2\u0087\u0089\5\5\3\2\u0088\u0085\3\2\2\2\u0089")
        buf.write("\u008c\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2")
        buf.write("\u008b\16\3\2\2\2\u008c\u008a\3\2\2\2\u008d\u0094\5\7")
        buf.write("\4\2\u008e\u0090\7a\2\2\u008f\u008e\3\2\2\2\u008f\u0090")
        buf.write("\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0093\5\7\4\2\u0092")
        buf.write("\u008f\3\2\2\2\u0093\u0096\3\2\2\2\u0094\u0092\3\2\2\2")
        buf.write("\u0094\u0095\3\2\2\2\u0095\20\3\2\2\2\u0096\u0094\3\2")
        buf.write("\2\2\u0097\u009e\5\t\5\2\u0098\u009a\7a\2\2\u0099\u0098")
        buf.write("\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009b\3\2\2\2\u009b")
        buf.write("\u009d\5\t\5\2\u009c\u0099\3\2\2\2\u009d\u00a0\3\2\2\2")
        buf.write("\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\22\3\2")
        buf.write("\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a8\7\62\2\2\u00a2\u00a3")
        buf.write("\t\3\2\2\u00a3\u00a9\5\13\6\2\u00a4\u00a5\t\4\2\2\u00a5")
        buf.write("\u00a9\5\21\t\2\u00a6\u00a7\t\5\2\2\u00a7\u00a9\5\r\7")
        buf.write("\2\u00a8\u00a2\3\2\2\2\u00a8\u00a4\3\2\2\2\u00a8\u00a6")
        buf.write("\3\2\2\2\u00a9\u00ac\3\2\2\2\u00aa\u00ac\5\17\b\2\u00ab")
        buf.write("\u00a1\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac\24\3\2\2\2\u00ad")
        buf.write("\u00b5\7$\2\2\u00ae\u00b4\n\6\2\2\u00af\u00b0\7^\2\2\u00b0")
        buf.write("\u00b4\7$\2\2\u00b1\u00b2\7^\2\2\u00b2\u00b4\5\27\f\2")
        buf.write("\u00b3\u00ae\3\2\2\2\u00b3\u00af\3\2\2\2\u00b3\u00b1\3")
        buf.write("\2\2\2\u00b4\u00b7\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b5\u00b6")
        buf.write("\3\2\2\2\u00b6\u00b8\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b8")
        buf.write("\u00b9\7$\2\2\u00b9\26\3\2\2\2\u00ba\u00d5\t\7\2\2\u00bb")
        buf.write("\u00bc\7z\2\2\u00bc\u00bd\5\t\5\2\u00bd\u00be\5\t\5\2")
        buf.write("\u00be\u00d5\3\2\2\2\u00bf\u00c0\7q\2\2\u00c0\u00c1\5")
        buf.write("\5\3\2\u00c1\u00c2\5\5\3\2\u00c2\u00c3\5\5\3\2\u00c3\u00d5")
        buf.write("\3\2\2\2\u00c4\u00c5\7w\2\2\u00c5\u00c6\5\t\5\2\u00c6")
        buf.write("\u00c7\5\t\5\2\u00c7\u00c8\5\t\5\2\u00c8\u00c9\5\t\5\2")
        buf.write("\u00c9\u00d5\3\2\2\2\u00ca\u00cb\7W\2\2\u00cb\u00cc\5")
        buf.write("\t\5\2\u00cc\u00cd\5\t\5\2\u00cd\u00ce\5\t\5\2\u00ce\u00cf")
        buf.write("\5\t\5\2\u00cf\u00d0\5\t\5\2\u00d0\u00d1\5\t\5\2\u00d1")
        buf.write("\u00d2\5\t\5\2\u00d2\u00d3\5\t\5\2\u00d3\u00d5\3\2\2\2")
        buf.write("\u00d4\u00ba\3\2\2\2\u00d4\u00bb\3\2\2\2\u00d4\u00bf\3")
        buf.write("\2\2\2\u00d4\u00c4\3\2\2\2\u00d4\u00ca\3\2\2\2\u00d5\30")
        buf.write("\3\2\2\2\u00d6\u00d8\t\b\2\2\u00d7\u00d6\3\2\2\2\u00d8")
        buf.write("\u00d9\3\2\2\2\u00d9\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2")
        buf.write("\u00da\u00db\3\2\2\2\u00db\u00dc\b\r\2\2\u00dc\32\3\2")
        buf.write("\2\2\u00dd\u00de\7#\2\2\u00de\u00df\7?\2\2\u00df\34\3")
        buf.write("\2\2\2\u00e0\u00e1\7d\2\2\u00e1\u00e2\7t\2\2\u00e2\u00e3")
        buf.write("\7g\2\2\u00e3\u00e4\7c\2\2\u00e4\u00e5\7m\2\2\u00e5\36")
        buf.write("\3\2\2\2\u00e6\u00e7\7e\2\2\u00e7\u00e8\7n\2\2\u00e8\u00e9")
        buf.write("\7c\2\2\u00e9\u00ea\7u\2\2\u00ea\u00eb\7u\2\2\u00eb \3")
        buf.write("\2\2\2\u00ec\u00ed\7.\2\2\u00ed\"\3\2\2\2\u00ee\u00ef")
        buf.write("\7e\2\2\u00ef\u00f0\7q\2\2\u00f0\u00f1\7p\2\2\u00f1\u00f2")
        buf.write("\7u\2\2\u00f2\u00f3\7v\2\2\u00f3\u00f4\7t\2\2\u00f4\u00f5")
        buf.write("\7w\2\2\u00f5\u00f6\7e\2\2\u00f6\u00f7\7v\2\2\u00f7\u00f8")
        buf.write("\7q\2\2\u00f8\u00f9\7t\2\2\u00f9$\3\2\2\2\u00fa\u00fb")
        buf.write("\7\60\2\2\u00fb&\3\2\2\2\u00fc\u00fd\7g\2\2\u00fd\u00fe")
        buf.write("\7n\2\2\u00fe\u00ff\7u\2\2\u00ff\u0100\7g\2\2\u0100(\3")
        buf.write("\2\2\2\u0101\u0102\7?\2\2\u0102*\3\2\2\2\u0103\u0104\7")
        buf.write("?\2\2\u0104\u0105\7?\2\2\u0105,\3\2\2\2\u0106\u0107\7")
        buf.write("g\2\2\u0107\u0108\7z\2\2\u0108\u0109\7v\2\2\u0109\u010a")
        buf.write("\7g\2\2\u010a\u010b\7p\2\2\u010b\u010c\7f\2\2\u010c\u010d")
        buf.write("\7u\2\2\u010d.\3\2\2\2\u010e\u010f\7h\2\2\u010f\u0110")
        buf.write("\7q\2\2\u0110\u0111\7t\2\2\u0111\60\3\2\2\2\u0112\u0113")
        buf.write("\7@\2\2\u0113\62\3\2\2\2\u0114\u0115\7@\2\2\u0115\u0116")
        buf.write("\7?\2\2\u0116\64\3\2\2\2\u0117\u0118\7k\2\2\u0118\u0119")
        buf.write("\7h\2\2\u0119\66\3\2\2\2\u011a\u011b\7k\2\2\u011b\u011c")
        buf.write("\7p\2\2\u011c\u011d\7v\2\2\u011d8\3\2\2\2\u011e\u011f")
        buf.write("\7}\2\2\u011f:\3\2\2\2\u0120\u0121\7]\2\2\u0121<\3\2\2")
        buf.write("\2\u0122\u0123\7>\2\2\u0123>\3\2\2\2\u0124\u0125\7>\2")
        buf.write("\2\u0125\u0126\7?\2\2\u0126@\3\2\2\2\u0127\u0128\7*\2")
        buf.write("\2\u0128B\3\2\2\2\u0129\u012a\7/\2\2\u012aD\3\2\2\2\u012b")
        buf.write("\u012c\7p\2\2\u012c\u012d\7g\2\2\u012d\u012e\7y\2\2\u012e")
        buf.write("F\3\2\2\2\u012f\u0130\7p\2\2\u0130\u0131\7w\2\2\u0131")
        buf.write("\u0132\7n\2\2\u0132\u0133\7n\2\2\u0133H\3\2\2\2\u0134")
        buf.write("\u0135\7\'\2\2\u0135J\3\2\2\2\u0136\u0137\7-\2\2\u0137")
        buf.write("L\3\2\2\2\u0138\u0139\7r\2\2\u0139\u013a\7t\2\2\u013a")
        buf.write("\u013b\7k\2\2\u013b\u013c\7p\2\2\u013c\u013d\7v\2\2\u013d")
        buf.write("N\3\2\2\2\u013e\u013f\7t\2\2\u013f\u0140\7g\2\2\u0140")
        buf.write("\u0141\7c\2\2\u0141\u0142\7f\2\2\u0142P\3\2\2\2\u0143")
        buf.write("\u0144\7t\2\2\u0144\u0145\7g\2\2\u0145\u0146\7v\2\2\u0146")
        buf.write("\u0147\7w\2\2\u0147\u0148\7t\2\2\u0148\u0149\7p\2\2\u0149")
        buf.write("R\3\2\2\2\u014a\u014b\7u\2\2\u014b\u014c\7w\2\2\u014c")
        buf.write("\u014d\7r\2\2\u014d\u014e\7g\2\2\u014e\u014f\7t\2\2\u014f")
        buf.write("T\3\2\2\2\u0150\u0151\7u\2\2\u0151\u0152\7v\2\2\u0152")
        buf.write("\u0153\7t\2\2\u0153\u0154\7k\2\2\u0154\u0155\7p\2\2\u0155")
        buf.write("\u0156\7i\2\2\u0156V\3\2\2\2\u0157\u0158\7\177\2\2\u0158")
        buf.write("X\3\2\2\2\u0159\u015a\7_\2\2\u015aZ\3\2\2\2\u015b\u015c")
        buf.write("\7+\2\2\u015c\\\3\2\2\2\u015d\u015e\7=\2\2\u015e^\3\2")
        buf.write("\2\2\u015f\u0160\7\61\2\2\u0160`\3\2\2\2\u0161\u0162\7")
        buf.write(",\2\2\u0162b\3\2\2\2\u0163\u0164\7x\2\2\u0164\u0165\7")
        buf.write("c\2\2\u0165\u0166\7t\2\2\u0166d\3\2\2\2\u0167\u016a\5")
        buf.write("g\64\2\u0168\u016a\t\t\2\2\u0169\u0167\3\2\2\2\u0169\u0168")
        buf.write("\3\2\2\2\u016af\3\2\2\2\u016b\u016c\t\n\2\2\u016ch\3\2")
        buf.write("\2\2\u016d\u0171\5g\64\2\u016e\u0170\5e\63\2\u016f\u016e")
        buf.write("\3\2\2\2\u0170\u0173\3\2\2\2\u0171\u016f\3\2\2\2\u0171")
        buf.write("\u0172\3\2\2\2\u0172j\3\2\2\2\u0173\u0171\3\2\2\2\26\2")
        buf.write("osw{\u0080\u0085\u008a\u008f\u0094\u0099\u009e\u00a8\u00ab")
        buf.write("\u00b3\u00b5\u00d4\u00d9\u0169\u0171\3\b\2\2")
        return buf.getvalue()


class XPPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INT_L = 1
    STR_L = 2
    WS = 3
    BANG_EQUAL = 4
    BREAK = 5
    CLASS = 6
    COMMA = 7
    CTOR = 8
    DOT = 9
    ELSE = 10
    EQUAL = 11
    EQUAL_EQUAL = 12
    EXTENDS = 13
    FOR = 14
    GREATER = 15
    GREATER_EQUAL = 16
    IF = 17
    INT = 18
    LBRACE = 19
    LBRACKET = 20
    LESS = 21
    LESS_EQUAL = 22
    LPARENS = 23
    MINUS = 24
    NEW = 25
    NULL = 26
    PERCENT = 27
    PLUS = 28
    PRINT = 29
    READ = 30
    RETURN = 31
    SUPER = 32
    STRING = 33
    RBRACE = 34
    RBRACKET = 35
    RPARENS = 36
    SEMICOLON = 37
    SLASH = 38
    STAR = 39
    VAR = 40
    ID = 41

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'!='", "'break'", "'class'", "','", "'constructor'", "'.'", 
            "'else'", "'='", "'=='", "'extends'", "'for'", "'>'", "'>='", 
            "'if'", "'int'", "'{'", "'['", "'<'", "'<='", "'('", "'-'", 
            "'new'", "'null'", "'%'", "'+'", "'print'", "'read'", "'return'", 
            "'super'", "'string'", "'}'", "']'", "')'", "';'", "'/'", "'*'", 
            "'var'" ]

    symbolicNames = [ "<INVALID>",
            "INT_L", "STR_L", "WS", "BANG_EQUAL", "BREAK", "CLASS", "COMMA", 
            "CTOR", "DOT", "ELSE", "EQUAL", "EQUAL_EQUAL", "EXTENDS", "FOR", 
            "GREATER", "GREATER_EQUAL", "IF", "INT", "LBRACE", "LBRACKET", 
            "LESS", "LESS_EQUAL", "LPARENS", "MINUS", "NEW", "NULL", "PERCENT", 
            "PLUS", "PRINT", "READ", "RETURN", "SUPER", "STRING", "RBRACE", 
            "RBRACKET", "RPARENS", "SEMICOLON", "SLASH", "STAR", "VAR", 
            "ID" ]

    ruleNames = [ "BIN_DIG", "OCT_DIG", "DEC_DIG", "HEX_DIG", "BIN_L", "OCT_L", 
                  "DEC_L", "HEX_L", "INT_L", "STR_L", "ESC_SEQ", "WS", "BANG_EQUAL", 
                  "BREAK", "CLASS", "COMMA", "CTOR", "DOT", "ELSE", "EQUAL", 
                  "EQUAL_EQUAL", "EXTENDS", "FOR", "GREATER", "GREATER_EQUAL", 
                  "IF", "INT", "LBRACE", "LBRACKET", "LESS", "LESS_EQUAL", 
                  "LPARENS", "MINUS", "NEW", "NULL", "PERCENT", "PLUS", 
                  "PRINT", "READ", "RETURN", "SUPER", "STRING", "RBRACE", 
                  "RBRACKET", "RPARENS", "SEMICOLON", "SLASH", "STAR", "VAR", 
                  "NameChar", "NameStartChar", "ID" ]

    grammarFileName = "XPPLexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


