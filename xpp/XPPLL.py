# Generated from /home/henry/Workspace/chab/XPPLL.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+")
        buf.write("\u013e\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\2\5\2:\n\2\3\3\3\3")
        buf.write("\3\3\5\3?\n\3\3\4\3\4\3\4\5\4D\n\4\3\5\3\5\3\5\5\5I\n")
        buf.write("\5\3\6\3\6\3\6\5\6N\n\6\3\7\3\7\3\7\3\7\5\7T\n\7\3\b\3")
        buf.write("\b\3\b\3\b\3\b\5\b[\n\b\3\t\3\t\3\t\3\t\5\ta\n\t\3\n\3")
        buf.write("\n\3\n\3\n\3\n\5\nh\n\n\3\13\3\13\5\13l\n\13\3\f\3\f\3")
        buf.write("\f\3\f\7\fr\n\f\f\f\16\fu\13\f\5\fw\n\f\3\f\3\f\5\f{\n")
        buf.write("\f\3\f\3\f\3\f\3\f\5\f\u0081\n\f\3\f\3\f\3\f\5\f\u0086")
        buf.write("\n\f\5\f\u0088\n\f\3\r\3\r\3\16\3\16\3\16\3\16\3\16\7")
        buf.write("\16\u0091\n\16\f\16\16\16\u0094\13\16\3\16\3\16\3\17\3")
        buf.write("\17\3\17\3\17\3\17\7\17\u009d\n\17\f\17\16\17\u00a0\13")
        buf.write("\17\3\20\3\20\7\20\u00a4\n\20\f\20\16\20\u00a7\13\20\3")
        buf.write("\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\7\22\u00b1\n\22")
        buf.write("\f\22\16\22\u00b4\13\22\5\22\u00b6\n\22\3\22\3\22\3\22")
        buf.write("\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\26")
        buf.write("\3\26\3\26\3\26\5\26\u00c9\n\26\3\26\3\26\7\26\u00cd\n")
        buf.write("\26\f\26\16\26\u00d0\13\26\3\26\7\26\u00d3\n\26\f\26\16")
        buf.write("\26\u00d6\13\26\3\26\7\26\u00d9\n\26\f\26\16\26\u00dc")
        buf.write("\13\26\3\26\7\26\u00df\n\26\f\26\16\26\u00e2\13\26\3\26")
        buf.write("\3\26\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\5\30\u00f2\n\30\3\30\3\30\3\30\5\30\u00f7\n")
        buf.write("\30\3\30\3\30\5\30\u00fb\n\30\3\30\3\30\5\30\u00ff\n\30")
        buf.write("\3\30\3\30\3\30\3\30\5\30\u0105\n\30\3\30\3\30\3\30\3")
        buf.write("\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\7\30")
        buf.write("\u0114\n\30\f\30\16\30\u0117\13\30\3\30\3\30\3\30\3\30")
        buf.write("\7\30\u011d\n\30\f\30\16\30\u0120\13\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\5\30\u0128\n\30\3\31\7\31\u012b\n\31\f")
        buf.write("\31\16\31\u012e\13\31\3\31\3\31\3\32\3\32\3\32\3\32\3")
        buf.write("\32\3\33\6\33\u0138\n\33\r\33\16\33\u0139\3\33\3\33\3")
        buf.write("\33\2\2\34\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$")
        buf.write("&(*,.\60\62\64\2\b\4\2\21\22\27\30\4\2\6\6\16\16\4\2\32")
        buf.write("\32\36\36\4\2\35\35()\4\2\3\4\34\34\5\2\24\24##++\2\u0153")
        buf.write("\2\66\3\2\2\2\4;\3\2\2\2\6@\3\2\2\2\bE\3\2\2\2\nM\3\2")
        buf.write("\2\2\fS\3\2\2\2\16Z\3\2\2\2\20\\\3\2\2\2\22g\3\2\2\2\24")
        buf.write("i\3\2\2\2\26\u0087\3\2\2\2\30\u0089\3\2\2\2\32\u008b\3")
        buf.write("\2\2\2\34\u0097\3\2\2\2\36\u00a1\3\2\2\2 \u00a8\3\2\2")
        buf.write("\2\"\u00ac\3\2\2\2$\u00ba\3\2\2\2&\u00be\3\2\2\2(\u00c1")
        buf.write("\3\2\2\2*\u00c4\3\2\2\2,\u00e5\3\2\2\2.\u0127\3\2\2\2")
        buf.write("\60\u012c\3\2\2\2\62\u0131\3\2\2\2\64\u0137\3\2\2\2\66")
        buf.write("9\5\4\3\2\678\t\2\2\28:\5\2\2\29\67\3\2\2\29:\3\2\2\2")
        buf.write(":\3\3\2\2\2;>\5\6\4\2<=\t\3\2\2=?\5\4\3\2><\3\2\2\2>?")
        buf.write("\3\2\2\2?\5\3\2\2\2@C\5\b\5\2AB\t\4\2\2BD\5\6\4\2CA\3")
        buf.write("\2\2\2CD\3\2\2\2D\7\3\2\2\2EH\5\n\6\2FG\t\5\2\2GI\5\b")
        buf.write("\5\2HF\3\2\2\2HI\3\2\2\2I\t\3\2\2\2JN\5\f\7\2KL\t\4\2")
        buf.write("\2LN\5\f\7\2MJ\3\2\2\2MK\3\2\2\2N\13\3\2\2\2OT\5\24\13")
        buf.write("\2PT\5\16\b\2QR\7\33\2\2RT\5\22\n\2SO\3\2\2\2SP\3\2\2")
        buf.write("\2SQ\3\2\2\2T\r\3\2\2\2U[\t\6\2\2VW\7\31\2\2WX\5\6\4\2")
        buf.write("XY\7&\2\2Y[\3\2\2\2ZU\3\2\2\2ZV\3\2\2\2[\17\3\2\2\2\\")
        buf.write("]\7\26\2\2]^\7\3\2\2^`\7%\2\2_a\5\20\t\2`_\3\2\2\2`a\3")
        buf.write("\2\2\2a\21\3\2\2\2bh\5\24\13\2cd\7#\2\2dh\5\20\t\2ef\7")
        buf.write("\24\2\2fh\5\20\t\2gb\3\2\2\2gc\3\2\2\2ge\3\2\2\2h\23\3")
        buf.write("\2\2\2ik\7+\2\2jl\5\26\f\2kj\3\2\2\2kl\3\2\2\2l\25\3\2")
        buf.write("\2\2mv\7\31\2\2ns\5\6\4\2op\7\t\2\2pr\5\6\4\2qo\3\2\2")
        buf.write("\2ru\3\2\2\2sq\3\2\2\2st\3\2\2\2tw\3\2\2\2us\3\2\2\2v")
        buf.write("n\3\2\2\2vw\3\2\2\2wx\3\2\2\2xz\7&\2\2y{\5\26\f\2zy\3")
        buf.write("\2\2\2z{\3\2\2\2{\u0088\3\2\2\2|}\7\26\2\2}~\5\6\4\2~")
        buf.write("\u0080\7%\2\2\177\u0081\5\26\f\2\u0080\177\3\2\2\2\u0080")
        buf.write("\u0081\3\2\2\2\u0081\u0088\3\2\2\2\u0082\u0083\7\13\2")
        buf.write("\2\u0083\u0085\7+\2\2\u0084\u0086\5\26\f\2\u0085\u0084")
        buf.write("\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0088\3\2\2\2\u0087")
        buf.write("m\3\2\2\2\u0087|\3\2\2\2\u0087\u0082\3\2\2\2\u0088\27")
        buf.write("\3\2\2\2\u0089\u008a\t\7\2\2\u008a\31\3\2\2\2\u008b\u008c")
        buf.write("\7*\2\2\u008c\u008d\5\30\r\2\u008d\u0092\5\36\20\2\u008e")
        buf.write("\u008f\7\t\2\2\u008f\u0091\5\36\20\2\u0090\u008e\3\2\2")
        buf.write("\2\u0091\u0094\3\2\2\2\u0092\u0090\3\2\2\2\u0092\u0093")
        buf.write("\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0092\3\2\2\2\u0095")
        buf.write("\u0096\7\'\2\2\u0096\33\3\2\2\2\u0097\u0098\7*\2\2\u0098")
        buf.write("\u0099\5\30\r\2\u0099\u009e\5\36\20\2\u009a\u009b\7\t")
        buf.write("\2\2\u009b\u009d\5\36\20\2\u009c\u009a\3\2\2\2\u009d\u00a0")
        buf.write("\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f")
        buf.write("\35\3\2\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a5\7+\2\2\u00a2")
        buf.write("\u00a4\5 \21\2\u00a3\u00a2\3\2\2\2\u00a4\u00a7\3\2\2\2")
        buf.write("\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\37\3\2")
        buf.write("\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00a9\7\26\2\2\u00a9\u00aa")
        buf.write("\7\3\2\2\u00aa\u00ab\7%\2\2\u00ab!\3\2\2\2\u00ac\u00b5")
        buf.write("\7\31\2\2\u00ad\u00b2\5&\24\2\u00ae\u00af\7\t\2\2\u00af")
        buf.write("\u00b1\5&\24\2\u00b0\u00ae\3\2\2\2\u00b1\u00b4\3\2\2\2")
        buf.write("\u00b2\u00b0\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b6\3")
        buf.write("\2\2\2\u00b4\u00b2\3\2\2\2\u00b5\u00ad\3\2\2\2\u00b5\u00b6")
        buf.write("\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b8\7&\2\2\u00b8")
        buf.write("\u00b9\5.\30\2\u00b9#\3\2\2\2\u00ba\u00bb\5\30\r\2\u00bb")
        buf.write("\u00bc\7+\2\2\u00bc\u00bd\5\"\22\2\u00bd%\3\2\2\2\u00be")
        buf.write("\u00bf\5\30\r\2\u00bf\u00c0\7+\2\2\u00c0\'\3\2\2\2\u00c1")
        buf.write("\u00c2\7\n\2\2\u00c2\u00c3\5\"\22\2\u00c3)\3\2\2\2\u00c4")
        buf.write("\u00c5\7\b\2\2\u00c5\u00c8\7+\2\2\u00c6\u00c7\7\17\2\2")
        buf.write("\u00c7\u00c9\7+\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3")
        buf.write("\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00ce\7\25\2\2\u00cb")
        buf.write("\u00cd\5*\26\2\u00cc\u00cb\3\2\2\2\u00cd\u00d0\3\2\2\2")
        buf.write("\u00ce\u00cc\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d4\3")
        buf.write("\2\2\2\u00d0\u00ce\3\2\2\2\u00d1\u00d3\5\32\16\2\u00d2")
        buf.write("\u00d1\3\2\2\2\u00d3\u00d6\3\2\2\2\u00d4\u00d2\3\2\2\2")
        buf.write("\u00d4\u00d5\3\2\2\2\u00d5\u00da\3\2\2\2\u00d6\u00d4\3")
        buf.write("\2\2\2\u00d7\u00d9\5(\25\2\u00d8\u00d7\3\2\2\2\u00d9\u00dc")
        buf.write("\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db")
        buf.write("\u00e0\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd\u00df\5$\23\2")
        buf.write("\u00de\u00dd\3\2\2\2\u00df\u00e2\3\2\2\2\u00e0\u00de\3")
        buf.write("\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e3\3\2\2\2\u00e2\u00e0")
        buf.write("\3\2\2\2\u00e3\u00e4\7$\2\2\u00e4+\3\2\2\2\u00e5\u00e6")
        buf.write("\5\24\13\2\u00e6\u00e7\7\r\2\2\u00e7\u00e8\5\2\2\2\u00e8")
        buf.write("-\3\2\2\2\u00e9\u0128\5\32\16\2\u00ea\u00eb\7\23\2\2\u00eb")
        buf.write("\u00ec\7\31\2\2\u00ec\u00ed\5\6\4\2\u00ed\u00ee\7&\2\2")
        buf.write("\u00ee\u00f1\5.\30\2\u00ef\u00f0\7\f\2\2\u00f0\u00f2\5")
        buf.write(".\30\2\u00f1\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u0128")
        buf.write("\3\2\2\2\u00f3\u00f4\7\20\2\2\u00f4\u00f6\7\31\2\2\u00f5")
        buf.write("\u00f7\5,\27\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2\2\2")
        buf.write("\u00f7\u00f8\3\2\2\2\u00f8\u00fa\7\'\2\2\u00f9\u00fb\5")
        buf.write("\6\4\2\u00fa\u00f9\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fc")
        buf.write("\3\2\2\2\u00fc\u00fe\7\'\2\2\u00fd\u00ff\5,\27\2\u00fe")
        buf.write("\u00fd\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u0100\3\2\2\2")
        buf.write("\u0100\u0101\7&\2\2\u0101\u0128\5.\30\2\u0102\u0104\7")
        buf.write("!\2\2\u0103\u0105\5\6\4\2\u0104\u0103\3\2\2\2\u0104\u0105")
        buf.write("\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0128\7\'\2\2\u0107")
        buf.write("\u0108\7\7\2\2\u0108\u0128\7\'\2\2\u0109\u010a\7\37\2")
        buf.write("\2\u010a\u010b\5\6\4\2\u010b\u010c\7\'\2\2\u010c\u0128")
        buf.write("\3\2\2\2\u010d\u010e\7 \2\2\u010e\u010f\5\24\13\2\u010f")
        buf.write("\u0110\7\'\2\2\u0110\u0128\3\2\2\2\u0111\u0115\7\25\2")
        buf.write("\2\u0112\u0114\5.\30\2\u0113\u0112\3\2\2\2\u0114\u0117")
        buf.write("\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116")
        buf.write("\u0118\3\2\2\2\u0117\u0115\3\2\2\2\u0118\u0128\7$\2\2")
        buf.write("\u0119\u011a\7\"\2\2\u011a\u011e\7\31\2\2\u011b\u011d")
        buf.write("\5\6\4\2\u011c\u011b\3\2\2\2\u011d\u0120\3\2\2\2\u011e")
        buf.write("\u011c\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0121\3\2\2\2")
        buf.write("\u0120\u011e\3\2\2\2\u0121\u0122\7&\2\2\u0122\u0128\7")
        buf.write("\'\2\2\u0123\u0124\5,\27\2\u0124\u0125\7\'\2\2\u0125\u0128")
        buf.write("\3\2\2\2\u0126\u0128\7\'\2\2\u0127\u00e9\3\2\2\2\u0127")
        buf.write("\u00ea\3\2\2\2\u0127\u00f3\3\2\2\2\u0127\u0102\3\2\2\2")
        buf.write("\u0127\u0107\3\2\2\2\u0127\u0109\3\2\2\2\u0127\u010d\3")
        buf.write("\2\2\2\u0127\u0111\3\2\2\2\u0127\u0119\3\2\2\2\u0127\u0123")
        buf.write("\3\2\2\2\u0127\u0126\3\2\2\2\u0128/\3\2\2\2\u0129\u012b")
        buf.write("\5*\26\2\u012a\u0129\3\2\2\2\u012b\u012e\3\2\2\2\u012c")
        buf.write("\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d\u012f\3\2\2\2")
        buf.write("\u012e\u012c\3\2\2\2\u012f\u0130\7\2\2\3\u0130\61\3\2")
        buf.write("\2\2\u0131\u0132\5\6\4\2\u0132\u0133\7\'\2\2\u0133\u0134")
        buf.write("\5\34\17\2\u0134\u0135\7\2\2\3\u0135\63\3\2\2\2\u0136")
        buf.write("\u0138\5.\30\2\u0137\u0136\3\2\2\2\u0138\u0139\3\2\2\2")
        buf.write("\u0139\u0137\3\2\2\2\u0139\u013a\3\2\2\2\u013a\u013b\3")
        buf.write("\2\2\2\u013b\u013c\7\2\2\3\u013c\65\3\2\2\2&9>CHMSZ`g")
        buf.write("ksvz\u0080\u0085\u0087\u0092\u009e\u00a5\u00b2\u00b5\u00c8")
        buf.write("\u00ce\u00d4\u00da\u00e0\u00f1\u00f6\u00fa\u00fe\u0104")
        buf.write("\u0115\u011e\u0127\u012c\u0139")
        return buf.getvalue()


class XPPLL ( Parser ):

    grammarFileName = "XPPLL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'!='", "'break'", "'class'", "','", "'constructor'", 
                     "'.'", "'else'", "'='", "'=='", "'extends'", "'for'", 
                     "'>'", "'>='", "'if'", "'int'", "'{'", "'['", "'<'", 
                     "'<='", "'('", "'-'", "'new'", "'null'", "'%'", "'+'", 
                     "'print'", "'read'", "'return'", "'super'", "'string'", 
                     "'}'", "']'", "')'", "';'", "'/'", "'*'", "'var'" ]

    symbolicNames = [ "<INVALID>", "INT_L", "STR_L", "WS", "BANG_EQUAL", 
                      "BREAK", "CLASS", "COMMA", "CTOR", "DOT", "ELSE", 
                      "EQUAL", "EQUAL_EQUAL", "EXTENDS", "FOR", "GREATER", 
                      "GREATER_EQUAL", "IF", "INT", "LBRACE", "LBRACKET", 
                      "LESS", "LESS_EQUAL", "LPARENS", "MINUS", "NEW", "NULL", 
                      "PERCENT", "PLUS", "PRINT", "READ", "RETURN", "SUPER", 
                      "STRING", "RBRACE", "RBRACKET", "RPARENS", "SEMICOLON", 
                      "SLASH", "STAR", "VAR", "ID" ]

    RULE_expr = 0
    RULE_equality = 1
    RULE_add = 2
    RULE_mul = 3
    RULE_unary = 4
    RULE_primary = 5
    RULE_factor = 6
    RULE_array_alloc = 7
    RULE_alloc = 8
    RULE_access = 9
    RULE_access_aux = 10
    RULE_type_name = 11
    RULE_var_decl = 12
    RULE_dec = 13
    RULE_name_decl = 14
    RULE_dimension = 15
    RULE_meth_body = 16
    RULE_meth_decl = 17
    RULE_param = 18
    RULE_ctor_decl = 19
    RULE_cls_decl = 20
    RULE_assign = 21
    RULE_stmt = 22
    RULE_program = 23
    RULE_as2 = 24
    RULE_asem2 = 25

    ruleNames =  [ "expr", "equality", "add", "mul", "unary", "primary", 
                   "factor", "array_alloc", "alloc", "access", "access_aux", 
                   "type_name", "var_decl", "dec", "name_decl", "dimension", 
                   "meth_body", "meth_decl", "param", "ctor_decl", "cls_decl", 
                   "assign", "stmt", "program", "as2", "asem2" ]

    EOF = Token.EOF
    INT_L=1
    STR_L=2
    WS=3
    BANG_EQUAL=4
    BREAK=5
    CLASS=6
    COMMA=7
    CTOR=8
    DOT=9
    ELSE=10
    EQUAL=11
    EQUAL_EQUAL=12
    EXTENDS=13
    FOR=14
    GREATER=15
    GREATER_EQUAL=16
    IF=17
    INT=18
    LBRACE=19
    LBRACKET=20
    LESS=21
    LESS_EQUAL=22
    LPARENS=23
    MINUS=24
    NEW=25
    NULL=26
    PERCENT=27
    PLUS=28
    PRINT=29
    READ=30
    RETURN=31
    SUPER=32
    STRING=33
    RBRACE=34
    RBRACKET=35
    RPARENS=36
    SEMICOLON=37
    SLASH=38
    STAR=39
    VAR=40
    ID=41

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token

        def equality(self):
            return self.getTypedRuleContext(XPPLL.EqualityContext,0)


        def expr(self):
            return self.getTypedRuleContext(XPPLL.ExprContext,0)


        def LESS(self):
            return self.getToken(XPPLL.LESS, 0)

        def LESS_EQUAL(self):
            return self.getToken(XPPLL.LESS_EQUAL, 0)

        def GREATER(self):
            return self.getToken(XPPLL.GREATER, 0)

        def GREATER_EQUAL(self):
            return self.getToken(XPPLL.GREATER_EQUAL, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = XPPLL.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.equality()
            self.state = 55
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.GREATER) | (1 << XPPLL.GREATER_EQUAL) | (1 << XPPLL.LESS) | (1 << XPPLL.LESS_EQUAL))) != 0):
                self.state = 53
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.GREATER) | (1 << XPPLL.GREATER_EQUAL) | (1 << XPPLL.LESS) | (1 << XPPLL.LESS_EQUAL))) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 54
                self.expr()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualityContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # AddContext
            self.op = None # Token
            self.rhs = None # EqualityContext

        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def equality(self):
            return self.getTypedRuleContext(XPPLL.EqualityContext,0)


        def EQUAL_EQUAL(self):
            return self.getToken(XPPLL.EQUAL_EQUAL, 0)

        def BANG_EQUAL(self):
            return self.getToken(XPPLL.BANG_EQUAL, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_equality

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality" ):
                listener.enterEquality(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality" ):
                listener.exitEquality(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquality" ):
                return visitor.visitEquality(self)
            else:
                return visitor.visitChildren(self)




    def equality(self):

        localctx = XPPLL.EqualityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_equality)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 57
            localctx.lhs = self.add()
            self.state = 60
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLL.BANG_EQUAL or _la==XPPLL.EQUAL_EQUAL:
                self.state = 58
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLL.BANG_EQUAL or _la==XPPLL.EQUAL_EQUAL):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 59
                localctx.rhs = self.equality()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AddContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # MulContext
            self.op = None # Token
            self.rhs = None # AddContext

        def mul(self):
            return self.getTypedRuleContext(XPPLL.MulContext,0)


        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def PLUS(self):
            return self.getToken(XPPLL.PLUS, 0)

        def MINUS(self):
            return self.getToken(XPPLL.MINUS, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_add

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdd" ):
                listener.enterAdd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdd" ):
                listener.exitAdd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAdd" ):
                return visitor.visitAdd(self)
            else:
                return visitor.visitChildren(self)




    def add(self):

        localctx = XPPLL.AddContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_add)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            localctx.lhs = self.mul()
            self.state = 65
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 63
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLL.MINUS or _la==XPPLL.PLUS):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 64
                localctx.rhs = self.add()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MulContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # UnaryContext
            self.op = None # Token
            self.rhs = None # MulContext

        def unary(self):
            return self.getTypedRuleContext(XPPLL.UnaryContext,0)


        def mul(self):
            return self.getTypedRuleContext(XPPLL.MulContext,0)


        def STAR(self):
            return self.getToken(XPPLL.STAR, 0)

        def SLASH(self):
            return self.getToken(XPPLL.SLASH, 0)

        def PERCENT(self):
            return self.getToken(XPPLL.PERCENT, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_mul

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMul" ):
                listener.enterMul(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMul" ):
                listener.exitMul(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMul" ):
                return visitor.visitMul(self)
            else:
                return visitor.visitChildren(self)




    def mul(self):

        localctx = XPPLL.MulContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_mul)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 67
            localctx.lhs = self.unary()
            self.state = 70
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.PERCENT) | (1 << XPPLL.SLASH) | (1 << XPPLL.STAR))) != 0):
                self.state = 68
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.PERCENT) | (1 << XPPLL.SLASH) | (1 << XPPLL.STAR))) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 69
                localctx.rhs = self.mul()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token
            self.rhs = None # PrimaryContext

        def primary(self):
            return self.getTypedRuleContext(XPPLL.PrimaryContext,0)


        def PLUS(self):
            return self.getToken(XPPLL.PLUS, 0)

        def MINUS(self):
            return self.getToken(XPPLL.MINUS, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_unary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnary" ):
                listener.enterUnary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnary" ):
                listener.exitUnary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnary" ):
                return visitor.visitUnary(self)
            else:
                return visitor.visitChildren(self)




    def unary(self):

        localctx = XPPLL.UnaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_unary)
        self._la = 0 # Token type
        try:
            self.state = 75
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.INT_L, XPPLL.STR_L, XPPLL.LPARENS, XPPLL.NEW, XPPLL.NULL, XPPLL.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 72
                self.primary()
                pass
            elif token in [XPPLL.MINUS, XPPLL.PLUS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 73
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLL.MINUS or _la==XPPLL.PLUS):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 74
                localctx.rhs = self.primary()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimaryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def access(self):
            return self.getTypedRuleContext(XPPLL.AccessContext,0)


        def factor(self):
            return self.getTypedRuleContext(XPPLL.FactorContext,0)


        def NEW(self):
            return self.getToken(XPPLL.NEW, 0)

        def alloc(self):
            return self.getTypedRuleContext(XPPLL.AllocContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_primary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimary" ):
                listener.enterPrimary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimary" ):
                listener.exitPrimary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimary" ):
                return visitor.visitPrimary(self)
            else:
                return visitor.visitChildren(self)




    def primary(self):

        localctx = XPPLL.PrimaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_primary)
        try:
            self.state = 81
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 77
                self.access()
                pass
            elif token in [XPPLL.INT_L, XPPLL.STR_L, XPPLL.LPARENS, XPPLL.NULL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 78
                self.factor()
                pass
            elif token in [XPPLL.NEW]:
                self.enterOuterAlt(localctx, 3)
                self.state = 79
                self.match(XPPLL.NEW)
                self.state = 80
                self.alloc()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLL.RULE_factor

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class LitContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT_L(self):
            return self.getToken(XPPLL.INT_L, 0)
        def STR_L(self):
            return self.getToken(XPPLL.STR_L, 0)
        def NULL(self):
            return self.getToken(XPPLL.NULL, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLit" ):
                listener.enterLit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLit" ):
                listener.exitLit(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLit" ):
                return visitor.visitLit(self)
            else:
                return visitor.visitChildren(self)


    class GroupContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)
        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)

        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGroup" ):
                listener.enterGroup(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGroup" ):
                listener.exitGroup(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGroup" ):
                return visitor.visitGroup(self)
            else:
                return visitor.visitChildren(self)



    def factor(self):

        localctx = XPPLL.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_factor)
        self._la = 0 # Token type
        try:
            self.state = 88
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.INT_L, XPPLL.STR_L, XPPLL.NULL]:
                localctx = XPPLL.LitContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 83
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT_L) | (1 << XPPLL.STR_L) | (1 << XPPLL.NULL))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [XPPLL.LPARENS]:
                localctx = XPPLL.GroupContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 84
                self.match(XPPLL.LPARENS)
                self.state = 85
                self.add()
                self.state = 86
                self.match(XPPLL.RPARENS)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Array_allocContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACKET(self):
            return self.getToken(XPPLL.LBRACKET, 0)

        def INT_L(self):
            return self.getToken(XPPLL.INT_L, 0)

        def RBRACKET(self):
            return self.getToken(XPPLL.RBRACKET, 0)

        def array_alloc(self):
            return self.getTypedRuleContext(XPPLL.Array_allocContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_array_alloc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_alloc" ):
                listener.enterArray_alloc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_alloc" ):
                listener.exitArray_alloc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_alloc" ):
                return visitor.visitArray_alloc(self)
            else:
                return visitor.visitChildren(self)




    def array_alloc(self):

        localctx = XPPLL.Array_allocContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_array_alloc)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.match(XPPLL.LBRACKET)
            self.state = 91
            self.match(XPPLL.INT_L)
            self.state = 92
            self.match(XPPLL.RBRACKET)
            self.state = 94
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLL.LBRACKET:
                self.state = 93
                self.array_alloc()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AllocContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.typ = None # Token

        def access(self):
            return self.getTypedRuleContext(XPPLL.AccessContext,0)


        def array_alloc(self):
            return self.getTypedRuleContext(XPPLL.Array_allocContext,0)


        def STRING(self):
            return self.getToken(XPPLL.STRING, 0)

        def INT(self):
            return self.getToken(XPPLL.INT, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_alloc

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAlloc" ):
                listener.enterAlloc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAlloc" ):
                listener.exitAlloc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAlloc" ):
                return visitor.visitAlloc(self)
            else:
                return visitor.visitChildren(self)




    def alloc(self):

        localctx = XPPLL.AllocContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_alloc)
        try:
            self.state = 101
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 96
                self.access()
                pass
            elif token in [XPPLL.STRING]:
                self.enterOuterAlt(localctx, 2)
                self.state = 97
                localctx.typ = self.match(XPPLL.STRING)
                self.state = 98
                self.array_alloc()
                pass
            elif token in [XPPLL.INT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 99
                localctx.typ = self.match(XPPLL.INT)
                self.state = 100
                self.array_alloc()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AccessContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(XPPLL.ID, 0)

        def access_aux(self):
            return self.getTypedRuleContext(XPPLL.Access_auxContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_access

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAccess" ):
                listener.enterAccess(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAccess" ):
                listener.exitAccess(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAccess" ):
                return visitor.visitAccess(self)
            else:
                return visitor.visitChildren(self)




    def access(self):

        localctx = XPPLL.AccessContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_access)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 103
            self.match(XPPLL.ID)
            self.state = 105
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.state = 104
                self.access_aux()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Access_auxContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLL.RULE_access_aux

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Get_itemContext(Access_auxContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.Access_auxContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LBRACKET(self):
            return self.getToken(XPPLL.LBRACKET, 0)
        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)

        def RBRACKET(self):
            return self.getToken(XPPLL.RBRACKET, 0)
        def access_aux(self):
            return self.getTypedRuleContext(XPPLL.Access_auxContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGet_item" ):
                listener.enterGet_item(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGet_item" ):
                listener.exitGet_item(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGet_item" ):
                return visitor.visitGet_item(self)
            else:
                return visitor.visitChildren(self)


    class Get_attrContext(Access_auxContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.Access_auxContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def DOT(self):
            return self.getToken(XPPLL.DOT, 0)
        def ID(self):
            return self.getToken(XPPLL.ID, 0)
        def access_aux(self):
            return self.getTypedRuleContext(XPPLL.Access_auxContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGet_attr" ):
                listener.enterGet_attr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGet_attr" ):
                listener.exitGet_attr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGet_attr" ):
                return visitor.visitGet_attr(self)
            else:
                return visitor.visitChildren(self)


    class Call_accessContext(Access_auxContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.Access_auxContext
            super().__init__(parser)
            self._add = None # AddContext
            self.args = list() # of AddContexts
            self.copyFrom(ctx)

        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)
        def access_aux(self):
            return self.getTypedRuleContext(XPPLL.Access_auxContext,0)

        def add(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.AddContext)
            else:
                return self.getTypedRuleContext(XPPLL.AddContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.COMMA)
            else:
                return self.getToken(XPPLL.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall_access" ):
                listener.enterCall_access(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall_access" ):
                listener.exitCall_access(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_access" ):
                return visitor.visitCall_access(self)
            else:
                return visitor.visitChildren(self)



    def access_aux(self):

        localctx = XPPLL.Access_auxContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_access_aux)
        self._la = 0 # Token type
        try:
            self.state = 133
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.LPARENS]:
                localctx = XPPLL.Call_accessContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 107
                self.match(XPPLL.LPARENS)
                self.state = 116
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT_L) | (1 << XPPLL.STR_L) | (1 << XPPLL.LPARENS) | (1 << XPPLL.MINUS) | (1 << XPPLL.NEW) | (1 << XPPLL.NULL) | (1 << XPPLL.PLUS) | (1 << XPPLL.ID))) != 0):
                    self.state = 108
                    localctx._add = self.add()
                    localctx.args.append(localctx._add)
                    self.state = 113
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==XPPLL.COMMA:
                        self.state = 109
                        self.match(XPPLL.COMMA)
                        self.state = 110
                        localctx._add = self.add()
                        localctx.args.append(localctx._add)
                        self.state = 115
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 118
                self.match(XPPLL.RPARENS)
                self.state = 120
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
                if la_ == 1:
                    self.state = 119
                    self.access_aux()


                pass
            elif token in [XPPLL.LBRACKET]:
                localctx = XPPLL.Get_itemContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 122
                self.match(XPPLL.LBRACKET)
                self.state = 123
                self.add()
                self.state = 124
                self.match(XPPLL.RBRACKET)
                self.state = 126
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
                if la_ == 1:
                    self.state = 125
                    self.access_aux()


                pass
            elif token in [XPPLL.DOT]:
                localctx = XPPLL.Get_attrContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 128
                self.match(XPPLL.DOT)
                self.state = 129
                self.match(XPPLL.ID)
                self.state = 131
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
                if la_ == 1:
                    self.state = 130
                    self.access_aux()


                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(XPPLL.ID, 0)

        def STRING(self):
            return self.getToken(XPPLL.STRING, 0)

        def INT(self):
            return self.getToken(XPPLL.INT, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_type_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_name" ):
                listener.enterType_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_name" ):
                listener.exitType_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_name" ):
                return visitor.visitType_name(self)
            else:
                return visitor.visitChildren(self)




    def type_name(self):

        localctx = XPPLL.Type_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_type_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 135
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT) | (1 << XPPLL.STRING) | (1 << XPPLL.ID))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Type_nameContext
            self._name_decl = None # Name_declContext
            self.variables = list() # of Name_declContexts

        def VAR(self):
            return self.getToken(XPPLL.VAR, 0)

        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)

        def type_name(self):
            return self.getTypedRuleContext(XPPLL.Type_nameContext,0)


        def name_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Name_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Name_declContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.COMMA)
            else:
                return self.getToken(XPPLL.COMMA, i)

        def getRuleIndex(self):
            return XPPLL.RULE_var_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar_decl" ):
                listener.enterVar_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar_decl" ):
                listener.exitVar_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_decl" ):
                return visitor.visitVar_decl(self)
            else:
                return visitor.visitChildren(self)




    def var_decl(self):

        localctx = XPPLL.Var_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_var_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 137
            self.match(XPPLL.VAR)
            self.state = 138
            localctx.type_ = self.type_name()
            self.state = 139
            localctx._name_decl = self.name_decl()
            localctx.variables.append(localctx._name_decl)
            self.state = 144
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.COMMA:
                self.state = 140
                self.match(XPPLL.COMMA)
                self.state = 141
                localctx._name_decl = self.name_decl()
                localctx.variables.append(localctx._name_decl)
                self.state = 146
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 147
            self.match(XPPLL.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DecContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Type_nameContext
            self._name_decl = None # Name_declContext
            self.variables = list() # of Name_declContexts

        def VAR(self):
            return self.getToken(XPPLL.VAR, 0)

        def type_name(self):
            return self.getTypedRuleContext(XPPLL.Type_nameContext,0)


        def name_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Name_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Name_declContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.COMMA)
            else:
                return self.getToken(XPPLL.COMMA, i)

        def getRuleIndex(self):
            return XPPLL.RULE_dec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDec" ):
                listener.enterDec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDec" ):
                listener.exitDec(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDec" ):
                return visitor.visitDec(self)
            else:
                return visitor.visitChildren(self)




    def dec(self):

        localctx = XPPLL.DecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_dec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self.match(XPPLL.VAR)
            self.state = 150
            localctx.type_ = self.type_name()
            self.state = 151
            localctx._name_decl = self.name_decl()
            localctx.variables.append(localctx._name_decl)
            self.state = 156
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.COMMA:
                self.state = 152
                self.match(XPPLL.COMMA)
                self.state = 153
                localctx._name_decl = self.name_decl()
                localctx.variables.append(localctx._name_decl)
                self.state = 158
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Name_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self._dimension = None # DimensionContext
            self.dims = list() # of DimensionContexts

        def ID(self):
            return self.getToken(XPPLL.ID, 0)

        def dimension(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.DimensionContext)
            else:
                return self.getTypedRuleContext(XPPLL.DimensionContext,i)


        def getRuleIndex(self):
            return XPPLL.RULE_name_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName_decl" ):
                listener.enterName_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName_decl" ):
                listener.exitName_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitName_decl" ):
                return visitor.visitName_decl(self)
            else:
                return visitor.visitChildren(self)




    def name_decl(self):

        localctx = XPPLL.Name_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_name_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 159
            localctx.name = self.match(XPPLL.ID)
            self.state = 163
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.LBRACKET:
                self.state = 160
                localctx._dimension = self.dimension()
                localctx.dims.append(localctx._dimension)
                self.state = 165
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DimensionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACKET(self):
            return self.getToken(XPPLL.LBRACKET, 0)

        def INT_L(self):
            return self.getToken(XPPLL.INT_L, 0)

        def RBRACKET(self):
            return self.getToken(XPPLL.RBRACKET, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_dimension

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDimension" ):
                listener.enterDimension(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDimension" ):
                listener.exitDimension(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDimension" ):
                return visitor.visitDimension(self)
            else:
                return visitor.visitChildren(self)




    def dimension(self):

        localctx = XPPLL.DimensionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_dimension)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 166
            self.match(XPPLL.LBRACKET)
            self.state = 167
            self.match(XPPLL.INT_L)
            self.state = 168
            self.match(XPPLL.RBRACKET)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meth_bodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._param = None # ParamContext
            self.params = list() # of ParamContexts
            self.body = None # StmtContext

        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)

        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)

        def stmt(self):
            return self.getTypedRuleContext(XPPLL.StmtContext,0)


        def param(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.ParamContext)
            else:
                return self.getTypedRuleContext(XPPLL.ParamContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.COMMA)
            else:
                return self.getToken(XPPLL.COMMA, i)

        def getRuleIndex(self):
            return XPPLL.RULE_meth_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeth_body" ):
                listener.enterMeth_body(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeth_body" ):
                listener.exitMeth_body(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeth_body" ):
                return visitor.visitMeth_body(self)
            else:
                return visitor.visitChildren(self)




    def meth_body(self):

        localctx = XPPLL.Meth_bodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_meth_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 170
            self.match(XPPLL.LPARENS)
            self.state = 179
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT) | (1 << XPPLL.STRING) | (1 << XPPLL.ID))) != 0):
                self.state = 171
                localctx._param = self.param()
                localctx.params.append(localctx._param)
                self.state = 176
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==XPPLL.COMMA:
                    self.state = 172
                    self.match(XPPLL.COMMA)
                    self.state = 173
                    localctx._param = self.param()
                    localctx.params.append(localctx._param)
                    self.state = 178
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 181
            self.match(XPPLL.RPARENS)
            self.state = 182
            localctx.body = self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meth_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ret_type = None # Type_nameContext
            self.name = None # Token
            self.body = None # Meth_bodyContext

        def type_name(self):
            return self.getTypedRuleContext(XPPLL.Type_nameContext,0)


        def ID(self):
            return self.getToken(XPPLL.ID, 0)

        def meth_body(self):
            return self.getTypedRuleContext(XPPLL.Meth_bodyContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_meth_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeth_decl" ):
                listener.enterMeth_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeth_decl" ):
                listener.exitMeth_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeth_decl" ):
                return visitor.visitMeth_decl(self)
            else:
                return visitor.visitChildren(self)




    def meth_decl(self):

        localctx = XPPLL.Meth_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_meth_decl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            localctx.ret_type = self.type_name()
            self.state = 185
            localctx.name = self.match(XPPLL.ID)
            self.state = 186
            localctx.body = self.meth_body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Type_nameContext
            self.name = None # Token

        def type_name(self):
            return self.getTypedRuleContext(XPPLL.Type_nameContext,0)


        def ID(self):
            return self.getToken(XPPLL.ID, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_param

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParam" ):
                listener.enterParam(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParam" ):
                listener.exitParam(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParam" ):
                return visitor.visitParam(self)
            else:
                return visitor.visitChildren(self)




    def param(self):

        localctx = XPPLL.ParamContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_param)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 188
            localctx.type_ = self.type_name()
            self.state = 189
            localctx.name = self.match(XPPLL.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ctor_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.body = None # Meth_bodyContext

        def CTOR(self):
            return self.getToken(XPPLL.CTOR, 0)

        def meth_body(self):
            return self.getTypedRuleContext(XPPLL.Meth_bodyContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_ctor_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCtor_decl" ):
                listener.enterCtor_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCtor_decl" ):
                listener.exitCtor_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCtor_decl" ):
                return visitor.visitCtor_decl(self)
            else:
                return visitor.visitChildren(self)




    def ctor_decl(self):

        localctx = XPPLL.Ctor_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_ctor_decl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 191
            self.match(XPPLL.CTOR)
            self.state = 192
            localctx.body = self.meth_body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Cls_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.super_class = None # Token
            self._cls_decl = None # Cls_declContext
            self.nested = list() # of Cls_declContexts
            self._var_decl = None # Var_declContext
            self.variables = list() # of Var_declContexts
            self._ctor_decl = None # Ctor_declContext
            self.ctors = list() # of Ctor_declContexts
            self._meth_decl = None # Meth_declContext
            self.meths = list() # of Meth_declContexts

        def CLASS(self):
            return self.getToken(XPPLL.CLASS, 0)

        def LBRACE(self):
            return self.getToken(XPPLL.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(XPPLL.RBRACE, 0)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.ID)
            else:
                return self.getToken(XPPLL.ID, i)

        def EXTENDS(self):
            return self.getToken(XPPLL.EXTENDS, 0)

        def cls_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Cls_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Cls_declContext,i)


        def var_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Var_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Var_declContext,i)


        def ctor_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Ctor_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Ctor_declContext,i)


        def meth_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Meth_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Meth_declContext,i)


        def getRuleIndex(self):
            return XPPLL.RULE_cls_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCls_decl" ):
                listener.enterCls_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCls_decl" ):
                listener.exitCls_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCls_decl" ):
                return visitor.visitCls_decl(self)
            else:
                return visitor.visitChildren(self)




    def cls_decl(self):

        localctx = XPPLL.Cls_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_cls_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.match(XPPLL.CLASS)
            self.state = 195
            localctx.name = self.match(XPPLL.ID)
            self.state = 198
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLL.EXTENDS:
                self.state = 196
                self.match(XPPLL.EXTENDS)
                self.state = 197
                localctx.super_class = self.match(XPPLL.ID)


            self.state = 200
            self.match(XPPLL.LBRACE)
            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.CLASS:
                self.state = 201
                localctx._cls_decl = self.cls_decl()
                localctx.nested.append(localctx._cls_decl)
                self.state = 206
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 210
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.VAR:
                self.state = 207
                localctx._var_decl = self.var_decl()
                localctx.variables.append(localctx._var_decl)
                self.state = 212
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.CTOR:
                self.state = 213
                localctx._ctor_decl = self.ctor_decl()
                localctx.ctors.append(localctx._ctor_decl)
                self.state = 218
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 222
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT) | (1 << XPPLL.STRING) | (1 << XPPLL.ID))) != 0):
                self.state = 219
                localctx._meth_decl = self.meth_decl()
                localctx.meths.append(localctx._meth_decl)
                self.state = 224
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 225
            self.match(XPPLL.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # AccessContext
            self.rhs = None # ExprContext

        def EQUAL(self):
            return self.getToken(XPPLL.EQUAL, 0)

        def access(self):
            return self.getTypedRuleContext(XPPLL.AccessContext,0)


        def expr(self):
            return self.getTypedRuleContext(XPPLL.ExprContext,0)


        def getRuleIndex(self):
            return XPPLL.RULE_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign" ):
                listener.enterAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign" ):
                listener.exitAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)




    def assign(self):

        localctx = XPPLL.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 227
            localctx.lhs = self.access()
            self.state = 228
            self.match(XPPLL.EQUAL)
            self.state = 229
            localctx.rhs = self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLL.RULE_stmt

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Read_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.des = None # AccessContext
            self.copyFrom(ctx)

        def READ(self):
            return self.getToken(XPPLL.READ, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)
        def access(self):
            return self.getTypedRuleContext(XPPLL.AccessContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRead_stmt" ):
                listener.enterRead_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRead_stmt" ):
                listener.exitRead_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRead_stmt" ):
                return visitor.visitRead_stmt(self)
            else:
                return visitor.visitChildren(self)


    class DeclContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def var_decl(self):
            return self.getTypedRuleContext(XPPLL.Var_declContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDecl" ):
                listener.enterDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDecl" ):
                listener.exitDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl" ):
                return visitor.visitDecl(self)
            else:
                return visitor.visitChildren(self)


    class Block_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self._stmt = None # StmtContext
            self.stmts = list() # of StmtContexts
            self.copyFrom(ctx)

        def LBRACE(self):
            return self.getToken(XPPLL.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(XPPLL.RBRACE, 0)
        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.StmtContext)
            else:
                return self.getTypedRuleContext(XPPLL.StmtContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlock_stmt" ):
                listener.enterBlock_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlock_stmt" ):
                listener.exitBlock_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock_stmt" ):
                return visitor.visitBlock_stmt(self)
            else:
                return visitor.visitChildren(self)


    class If_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.condition = None # AddContext
            self.then_branch = None # StmtContext
            self.else_branch = None # StmtContext
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(XPPLL.IF, 0)
        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)
        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.StmtContext)
            else:
                return self.getTypedRuleContext(XPPLL.StmtContext,i)

        def ELSE(self):
            return self.getToken(XPPLL.ELSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_stmt" ):
                listener.enterIf_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_stmt" ):
                listener.exitIf_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIf_stmt" ):
                return visitor.visitIf_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Break_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def BREAK(self):
            return self.getToken(XPPLL.BREAK, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBreak_stmt" ):
                listener.enterBreak_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBreak_stmt" ):
                listener.exitBreak_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreak_stmt" ):
                return visitor.visitBreak_stmt(self)
            else:
                return visitor.visitChildren(self)


    class For_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.assignment = None # AssignContext
            self.condition = None # AddContext
            self.step = None # AssignContext
            self.body = None # StmtContext
            self.copyFrom(ctx)

        def FOR(self):
            return self.getToken(XPPLL.FOR, 0)
        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)
        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLL.SEMICOLON)
            else:
                return self.getToken(XPPLL.SEMICOLON, i)
        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)
        def stmt(self):
            return self.getTypedRuleContext(XPPLL.StmtContext,0)

        def assign(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.AssignContext)
            else:
                return self.getTypedRuleContext(XPPLL.AssignContext,i)

        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFor_stmt" ):
                listener.enterFor_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFor_stmt" ):
                listener.exitFor_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFor_stmt" ):
                return visitor.visitFor_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Super_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self._add = None # AddContext
            self.args = list() # of AddContexts
            self.copyFrom(ctx)

        def SUPER(self):
            return self.getToken(XPPLL.SUPER, 0)
        def LPARENS(self):
            return self.getToken(XPPLL.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLL.RPARENS, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)
        def add(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.AddContext)
            else:
                return self.getTypedRuleContext(XPPLL.AddContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSuper_stmt" ):
                listener.enterSuper_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSuper_stmt" ):
                listener.exitSuper_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSuper_stmt" ):
                return visitor.visitSuper_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Print_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.val = None # AddContext
            self.copyFrom(ctx)

        def PRINT(self):
            return self.getToken(XPPLL.PRINT, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)
        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrint_stmt" ):
                listener.enterPrint_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrint_stmt" ):
                listener.exitPrint_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrint_stmt" ):
                return visitor.visitPrint_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Scolon_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScolon_stmt" ):
                listener.enterScolon_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScolon_stmt" ):
                listener.exitScolon_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScolon_stmt" ):
                return visitor.visitScolon_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Return_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.val = None # AddContext
            self.copyFrom(ctx)

        def RETURN(self):
            return self.getToken(XPPLL.RETURN, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)
        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReturn_stmt" ):
                listener.enterReturn_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReturn_stmt" ):
                listener.exitReturn_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturn_stmt" ):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Assign_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLL.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def assign(self):
            return self.getTypedRuleContext(XPPLL.AssignContext,0)

        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_stmt" ):
                listener.enterAssign_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_stmt" ):
                listener.exitAssign_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_stmt" ):
                return visitor.visitAssign_stmt(self)
            else:
                return visitor.visitChildren(self)



    def stmt(self):

        localctx = XPPLL.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_stmt)
        self._la = 0 # Token type
        try:
            self.state = 293
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLL.VAR]:
                localctx = XPPLL.DeclContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 231
                self.var_decl()
                pass
            elif token in [XPPLL.IF]:
                localctx = XPPLL.If_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 232
                self.match(XPPLL.IF)
                self.state = 233
                self.match(XPPLL.LPARENS)
                self.state = 234
                localctx.condition = self.add()
                self.state = 235
                self.match(XPPLL.RPARENS)
                self.state = 236
                localctx.then_branch = self.stmt()
                self.state = 239
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
                if la_ == 1:
                    self.state = 237
                    self.match(XPPLL.ELSE)
                    self.state = 238
                    localctx.else_branch = self.stmt()


                pass
            elif token in [XPPLL.FOR]:
                localctx = XPPLL.For_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 241
                self.match(XPPLL.FOR)
                self.state = 242
                self.match(XPPLL.LPARENS)
                self.state = 244
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==XPPLL.ID:
                    self.state = 243
                    localctx.assignment = self.assign()


                self.state = 246
                self.match(XPPLL.SEMICOLON)
                self.state = 248
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT_L) | (1 << XPPLL.STR_L) | (1 << XPPLL.LPARENS) | (1 << XPPLL.MINUS) | (1 << XPPLL.NEW) | (1 << XPPLL.NULL) | (1 << XPPLL.PLUS) | (1 << XPPLL.ID))) != 0):
                    self.state = 247
                    localctx.condition = self.add()


                self.state = 250
                self.match(XPPLL.SEMICOLON)
                self.state = 252
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==XPPLL.ID:
                    self.state = 251
                    localctx.step = self.assign()


                self.state = 254
                self.match(XPPLL.RPARENS)
                self.state = 255
                localctx.body = self.stmt()
                pass
            elif token in [XPPLL.RETURN]:
                localctx = XPPLL.Return_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 256
                self.match(XPPLL.RETURN)
                self.state = 258
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT_L) | (1 << XPPLL.STR_L) | (1 << XPPLL.LPARENS) | (1 << XPPLL.MINUS) | (1 << XPPLL.NEW) | (1 << XPPLL.NULL) | (1 << XPPLL.PLUS) | (1 << XPPLL.ID))) != 0):
                    self.state = 257
                    localctx.val = self.add()


                self.state = 260
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.BREAK]:
                localctx = XPPLL.Break_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 261
                self.match(XPPLL.BREAK)
                self.state = 262
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.PRINT]:
                localctx = XPPLL.Print_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 263
                self.match(XPPLL.PRINT)
                self.state = 264
                localctx.val = self.add()
                self.state = 265
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.READ]:
                localctx = XPPLL.Read_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 267
                self.match(XPPLL.READ)
                self.state = 268
                localctx.des = self.access()
                self.state = 269
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.LBRACE]:
                localctx = XPPLL.Block_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 271
                self.match(XPPLL.LBRACE)
                self.state = 275
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.BREAK) | (1 << XPPLL.FOR) | (1 << XPPLL.IF) | (1 << XPPLL.LBRACE) | (1 << XPPLL.PRINT) | (1 << XPPLL.READ) | (1 << XPPLL.RETURN) | (1 << XPPLL.SUPER) | (1 << XPPLL.SEMICOLON) | (1 << XPPLL.VAR) | (1 << XPPLL.ID))) != 0):
                    self.state = 272
                    localctx._stmt = self.stmt()
                    localctx.stmts.append(localctx._stmt)
                    self.state = 277
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 278
                self.match(XPPLL.RBRACE)
                pass
            elif token in [XPPLL.SUPER]:
                localctx = XPPLL.Super_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 279
                self.match(XPPLL.SUPER)
                self.state = 280
                self.match(XPPLL.LPARENS)
                self.state = 284
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.INT_L) | (1 << XPPLL.STR_L) | (1 << XPPLL.LPARENS) | (1 << XPPLL.MINUS) | (1 << XPPLL.NEW) | (1 << XPPLL.NULL) | (1 << XPPLL.PLUS) | (1 << XPPLL.ID))) != 0):
                    self.state = 281
                    localctx._add = self.add()
                    localctx.args.append(localctx._add)
                    self.state = 286
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 287
                self.match(XPPLL.RPARENS)
                self.state = 288
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.ID]:
                localctx = XPPLL.Assign_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 289
                self.assign()
                self.state = 290
                self.match(XPPLL.SEMICOLON)
                pass
            elif token in [XPPLL.SEMICOLON]:
                localctx = XPPLL.Scolon_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 292
                self.match(XPPLL.SEMICOLON)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._cls_decl = None # Cls_declContext
            self.classes = list() # of Cls_declContexts

        def EOF(self):
            return self.getToken(XPPLL.EOF, 0)

        def cls_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.Cls_declContext)
            else:
                return self.getTypedRuleContext(XPPLL.Cls_declContext,i)


        def getRuleIndex(self):
            return XPPLL.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = XPPLL.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 298
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLL.CLASS:
                self.state = 295
                localctx._cls_decl = self.cls_decl()
                localctx.classes.append(localctx._cls_decl)
                self.state = 300
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 301
            self.match(XPPLL.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class As2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def add(self):
            return self.getTypedRuleContext(XPPLL.AddContext,0)


        def SEMICOLON(self):
            return self.getToken(XPPLL.SEMICOLON, 0)

        def dec(self):
            return self.getTypedRuleContext(XPPLL.DecContext,0)


        def EOF(self):
            return self.getToken(XPPLL.EOF, 0)

        def getRuleIndex(self):
            return XPPLL.RULE_as2

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAs2" ):
                listener.enterAs2(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAs2" ):
                listener.exitAs2(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAs2" ):
                return visitor.visitAs2(self)
            else:
                return visitor.visitChildren(self)




    def as2(self):

        localctx = XPPLL.As2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_as2)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 303
            self.add()
            self.state = 304
            self.match(XPPLL.SEMICOLON)
            self.state = 305
            self.dec()
            self.state = 306
            self.match(XPPLL.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Asem2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(XPPLL.EOF, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLL.StmtContext)
            else:
                return self.getTypedRuleContext(XPPLL.StmtContext,i)


        def getRuleIndex(self):
            return XPPLL.RULE_asem2

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAsem2" ):
                listener.enterAsem2(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAsem2" ):
                listener.exitAsem2(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAsem2" ):
                return visitor.visitAsem2(self)
            else:
                return visitor.visitChildren(self)




    def asem2(self):

        localctx = XPPLL.Asem2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_asem2)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 309 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 308
                self.stmt()
                self.state = 311 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLL.BREAK) | (1 << XPPLL.FOR) | (1 << XPPLL.IF) | (1 << XPPLL.LBRACE) | (1 << XPPLL.PRINT) | (1 << XPPLL.READ) | (1 << XPPLL.RETURN) | (1 << XPPLL.SUPER) | (1 << XPPLL.SEMICOLON) | (1 << XPPLL.VAR) | (1 << XPPLL.ID))) != 0)):
                    break

            self.state = 313
            self.match(XPPLL.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





