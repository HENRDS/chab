# Generated from /home/henry/Workspace/chab/XPPLL.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .XPPLL import XPPLL
else:
    from XPPLL import XPPLL

# This class defines a complete listener for a parse tree produced by XPPLL.
class XPPLLListener(ParseTreeListener):

    # Enter a parse tree produced by XPPLL#expr.
    def enterExpr(self, ctx:XPPLL.ExprContext):
        pass

    # Exit a parse tree produced by XPPLL#expr.
    def exitExpr(self, ctx:XPPLL.ExprContext):
        pass


    # Enter a parse tree produced by XPPLL#equality.
    def enterEquality(self, ctx:XPPLL.EqualityContext):
        pass

    # Exit a parse tree produced by XPPLL#equality.
    def exitEquality(self, ctx:XPPLL.EqualityContext):
        pass


    # Enter a parse tree produced by XPPLL#add.
    def enterAdd(self, ctx:XPPLL.AddContext):
        pass

    # Exit a parse tree produced by XPPLL#add.
    def exitAdd(self, ctx:XPPLL.AddContext):
        pass


    # Enter a parse tree produced by XPPLL#mul.
    def enterMul(self, ctx:XPPLL.MulContext):
        pass

    # Exit a parse tree produced by XPPLL#mul.
    def exitMul(self, ctx:XPPLL.MulContext):
        pass


    # Enter a parse tree produced by XPPLL#unary.
    def enterUnary(self, ctx:XPPLL.UnaryContext):
        pass

    # Exit a parse tree produced by XPPLL#unary.
    def exitUnary(self, ctx:XPPLL.UnaryContext):
        pass


    # Enter a parse tree produced by XPPLL#primary.
    def enterPrimary(self, ctx:XPPLL.PrimaryContext):
        pass

    # Exit a parse tree produced by XPPLL#primary.
    def exitPrimary(self, ctx:XPPLL.PrimaryContext):
        pass


    # Enter a parse tree produced by XPPLL#lit.
    def enterLit(self, ctx:XPPLL.LitContext):
        pass

    # Exit a parse tree produced by XPPLL#lit.
    def exitLit(self, ctx:XPPLL.LitContext):
        pass


    # Enter a parse tree produced by XPPLL#group.
    def enterGroup(self, ctx:XPPLL.GroupContext):
        pass

    # Exit a parse tree produced by XPPLL#group.
    def exitGroup(self, ctx:XPPLL.GroupContext):
        pass


    # Enter a parse tree produced by XPPLL#array_alloc.
    def enterArray_alloc(self, ctx:XPPLL.Array_allocContext):
        pass

    # Exit a parse tree produced by XPPLL#array_alloc.
    def exitArray_alloc(self, ctx:XPPLL.Array_allocContext):
        pass


    # Enter a parse tree produced by XPPLL#alloc.
    def enterAlloc(self, ctx:XPPLL.AllocContext):
        pass

    # Exit a parse tree produced by XPPLL#alloc.
    def exitAlloc(self, ctx:XPPLL.AllocContext):
        pass


    # Enter a parse tree produced by XPPLL#access.
    def enterAccess(self, ctx:XPPLL.AccessContext):
        pass

    # Exit a parse tree produced by XPPLL#access.
    def exitAccess(self, ctx:XPPLL.AccessContext):
        pass


    # Enter a parse tree produced by XPPLL#call_access.
    def enterCall_access(self, ctx:XPPLL.Call_accessContext):
        pass

    # Exit a parse tree produced by XPPLL#call_access.
    def exitCall_access(self, ctx:XPPLL.Call_accessContext):
        pass


    # Enter a parse tree produced by XPPLL#get_item.
    def enterGet_item(self, ctx:XPPLL.Get_itemContext):
        pass

    # Exit a parse tree produced by XPPLL#get_item.
    def exitGet_item(self, ctx:XPPLL.Get_itemContext):
        pass


    # Enter a parse tree produced by XPPLL#get_attr.
    def enterGet_attr(self, ctx:XPPLL.Get_attrContext):
        pass

    # Exit a parse tree produced by XPPLL#get_attr.
    def exitGet_attr(self, ctx:XPPLL.Get_attrContext):
        pass


    # Enter a parse tree produced by XPPLL#type_name.
    def enterType_name(self, ctx:XPPLL.Type_nameContext):
        pass

    # Exit a parse tree produced by XPPLL#type_name.
    def exitType_name(self, ctx:XPPLL.Type_nameContext):
        pass


    # Enter a parse tree produced by XPPLL#var_decl.
    def enterVar_decl(self, ctx:XPPLL.Var_declContext):
        pass

    # Exit a parse tree produced by XPPLL#var_decl.
    def exitVar_decl(self, ctx:XPPLL.Var_declContext):
        pass


    # Enter a parse tree produced by XPPLL#dec.
    def enterDec(self, ctx:XPPLL.DecContext):
        pass

    # Exit a parse tree produced by XPPLL#dec.
    def exitDec(self, ctx:XPPLL.DecContext):
        pass


    # Enter a parse tree produced by XPPLL#name_decl.
    def enterName_decl(self, ctx:XPPLL.Name_declContext):
        pass

    # Exit a parse tree produced by XPPLL#name_decl.
    def exitName_decl(self, ctx:XPPLL.Name_declContext):
        pass


    # Enter a parse tree produced by XPPLL#dimension.
    def enterDimension(self, ctx:XPPLL.DimensionContext):
        pass

    # Exit a parse tree produced by XPPLL#dimension.
    def exitDimension(self, ctx:XPPLL.DimensionContext):
        pass


    # Enter a parse tree produced by XPPLL#meth_body.
    def enterMeth_body(self, ctx:XPPLL.Meth_bodyContext):
        pass

    # Exit a parse tree produced by XPPLL#meth_body.
    def exitMeth_body(self, ctx:XPPLL.Meth_bodyContext):
        pass


    # Enter a parse tree produced by XPPLL#meth_decl.
    def enterMeth_decl(self, ctx:XPPLL.Meth_declContext):
        pass

    # Exit a parse tree produced by XPPLL#meth_decl.
    def exitMeth_decl(self, ctx:XPPLL.Meth_declContext):
        pass


    # Enter a parse tree produced by XPPLL#param.
    def enterParam(self, ctx:XPPLL.ParamContext):
        pass

    # Exit a parse tree produced by XPPLL#param.
    def exitParam(self, ctx:XPPLL.ParamContext):
        pass


    # Enter a parse tree produced by XPPLL#ctor_decl.
    def enterCtor_decl(self, ctx:XPPLL.Ctor_declContext):
        pass

    # Exit a parse tree produced by XPPLL#ctor_decl.
    def exitCtor_decl(self, ctx:XPPLL.Ctor_declContext):
        pass


    # Enter a parse tree produced by XPPLL#cls_decl.
    def enterCls_decl(self, ctx:XPPLL.Cls_declContext):
        pass

    # Exit a parse tree produced by XPPLL#cls_decl.
    def exitCls_decl(self, ctx:XPPLL.Cls_declContext):
        pass


    # Enter a parse tree produced by XPPLL#assign.
    def enterAssign(self, ctx:XPPLL.AssignContext):
        pass

    # Exit a parse tree produced by XPPLL#assign.
    def exitAssign(self, ctx:XPPLL.AssignContext):
        pass


    # Enter a parse tree produced by XPPLL#decl.
    def enterDecl(self, ctx:XPPLL.DeclContext):
        pass

    # Exit a parse tree produced by XPPLL#decl.
    def exitDecl(self, ctx:XPPLL.DeclContext):
        pass


    # Enter a parse tree produced by XPPLL#if_stmt.
    def enterIf_stmt(self, ctx:XPPLL.If_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#if_stmt.
    def exitIf_stmt(self, ctx:XPPLL.If_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#for_stmt.
    def enterFor_stmt(self, ctx:XPPLL.For_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#for_stmt.
    def exitFor_stmt(self, ctx:XPPLL.For_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#return_stmt.
    def enterReturn_stmt(self, ctx:XPPLL.Return_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#return_stmt.
    def exitReturn_stmt(self, ctx:XPPLL.Return_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#break_stmt.
    def enterBreak_stmt(self, ctx:XPPLL.Break_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#break_stmt.
    def exitBreak_stmt(self, ctx:XPPLL.Break_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#print_stmt.
    def enterPrint_stmt(self, ctx:XPPLL.Print_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#print_stmt.
    def exitPrint_stmt(self, ctx:XPPLL.Print_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#read_stmt.
    def enterRead_stmt(self, ctx:XPPLL.Read_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#read_stmt.
    def exitRead_stmt(self, ctx:XPPLL.Read_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#block_stmt.
    def enterBlock_stmt(self, ctx:XPPLL.Block_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#block_stmt.
    def exitBlock_stmt(self, ctx:XPPLL.Block_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#super_stmt.
    def enterSuper_stmt(self, ctx:XPPLL.Super_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#super_stmt.
    def exitSuper_stmt(self, ctx:XPPLL.Super_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#assign_stmt.
    def enterAssign_stmt(self, ctx:XPPLL.Assign_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#assign_stmt.
    def exitAssign_stmt(self, ctx:XPPLL.Assign_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#scolon_stmt.
    def enterScolon_stmt(self, ctx:XPPLL.Scolon_stmtContext):
        pass

    # Exit a parse tree produced by XPPLL#scolon_stmt.
    def exitScolon_stmt(self, ctx:XPPLL.Scolon_stmtContext):
        pass


    # Enter a parse tree produced by XPPLL#program.
    def enterProgram(self, ctx:XPPLL.ProgramContext):
        pass

    # Exit a parse tree produced by XPPLL#program.
    def exitProgram(self, ctx:XPPLL.ProgramContext):
        pass


    # Enter a parse tree produced by XPPLL#as2.
    def enterAs2(self, ctx:XPPLL.As2Context):
        pass

    # Exit a parse tree produced by XPPLL#as2.
    def exitAs2(self, ctx:XPPLL.As2Context):
        pass


    # Enter a parse tree produced by XPPLL#asem2.
    def enterAsem2(self, ctx:XPPLL.Asem2Context):
        pass

    # Exit a parse tree produced by XPPLL#asem2.
    def exitAsem2(self, ctx:XPPLL.Asem2Context):
        pass


