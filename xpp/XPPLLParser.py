# Generated from /home/henry/Workspace/chab/XPPLL.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3*")
        buf.write("\u012e\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2")
        buf.write("\3\2\3\2\3\3\3\3\3\3\5\3\67\n\3\3\4\3\4\3\4\5\4<\n\4\3")
        buf.write("\5\3\5\3\5\5\5A\n\5\3\6\3\6\3\6\5\6F\n\6\3\7\5\7I\n\7")
        buf.write("\3\7\3\7\3\b\3\b\3\b\5\bP\n\b\3\t\3\t\3\t\3\t\3\t\5\t")
        buf.write("W\n\t\3\n\3\n\3\n\3\n\3\n\3\n\7\n_\n\n\f\n\16\nb\13\n")
        buf.write("\5\nd\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\6\nm\n\n\r\n\16")
        buf.write("\nn\5\nq\n\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13y\n\13")
        buf.write("\f\13\16\13|\13\13\5\13~\n\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\5\13\u0087\n\13\3\f\3\f\7\f\u008b\n\f\f\f\16")
        buf.write("\f\u008e\13\f\3\r\3\r\3\16\3\16\3\16\3\16\7\16\u0096\n")
        buf.write("\16\f\16\16\16\u0099\13\16\3\16\3\16\3\17\3\17\7\17\u009f")
        buf.write("\n\17\f\17\16\17\u00a2\13\17\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\21\7\21\u00ab\n\21\f\21\16\21\u00ae\13\21\5\21")
        buf.write("\u00b0\n\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3")
        buf.write("\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\5\25\u00c3")
        buf.write("\n\25\3\25\3\25\7\25\u00c7\n\25\f\25\16\25\u00ca\13\25")
        buf.write("\3\25\7\25\u00cd\n\25\f\25\16\25\u00d0\13\25\3\25\7\25")
        buf.write("\u00d3\n\25\f\25\16\25\u00d6\13\25\3\25\7\25\u00d9\n\25")
        buf.write("\f\25\16\25\u00dc\13\25\3\25\3\25\3\26\3\26\3\26\3\26")
        buf.write("\3\27\3\27\5\27\u00e6\n\27\3\27\3\27\3\27\3\27\3\27\3")
        buf.write("\27\3\27\5\27\u00ef\n\27\3\27\3\27\3\27\5\27\u00f4\n\27")
        buf.write("\3\27\3\27\5\27\u00f8\n\27\3\27\3\27\5\27\u00fc\n\27\3")
        buf.write("\27\3\27\3\27\3\27\5\27\u0102\n\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u0111")
        buf.write("\n\27\f\27\16\27\u0114\13\27\3\27\3\27\3\27\3\27\7\27")
        buf.write("\u011a\n\27\f\27\16\27\u011d\13\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\5\27\u0124\n\27\3\30\7\30\u0127\n\30\f\30\16\30")
        buf.write("\u012a\13\30\3\30\3\30\3\30\2\2\31\2\4\6\b\n\f\16\20\22")
        buf.write("\24\26\30\32\34\36 \"$&(*,.\2\b\4\2\21\22\27\30\4\2\6")
        buf.write("\6\16\16\4\2\32\32\36\36\4\2\35\35()\4\2\3\4\34\34\5\2")
        buf.write("\24\24##**\2\u0143\2\60\3\2\2\2\4\66\3\2\2\2\68\3\2\2")
        buf.write("\2\b=\3\2\2\2\nB\3\2\2\2\fH\3\2\2\2\16O\3\2\2\2\20V\3")
        buf.write("\2\2\2\22p\3\2\2\2\24\u0086\3\2\2\2\26\u0088\3\2\2\2\30")
        buf.write("\u008f\3\2\2\2\32\u0091\3\2\2\2\34\u009c\3\2\2\2\36\u00a3")
        buf.write("\3\2\2\2 \u00a6\3\2\2\2\"\u00b4\3\2\2\2$\u00b8\3\2\2\2")
        buf.write("&\u00bb\3\2\2\2(\u00be\3\2\2\2*\u00df\3\2\2\2,\u0123\3")
        buf.write("\2\2\2.\u0128\3\2\2\2\60\61\5\6\4\2\61\62\5\4\3\2\62\3")
        buf.write("\3\2\2\2\63\64\t\2\2\2\64\67\5\6\4\2\65\67\3\2\2\2\66")
        buf.write("\63\3\2\2\2\66\65\3\2\2\2\67\5\3\2\2\28;\5\b\5\29:\t\3")
        buf.write("\2\2:<\5\b\5\2;9\3\2\2\2;<\3\2\2\2<\7\3\2\2\2=@\5\n\6")
        buf.write("\2>?\t\4\2\2?A\5\n\6\2@>\3\2\2\2@A\3\2\2\2A\t\3\2\2\2")
        buf.write("BE\5\f\7\2CD\t\5\2\2DF\5\f\7\2EC\3\2\2\2EF\3\2\2\2F\13")
        buf.write("\3\2\2\2GI\t\4\2\2HG\3\2\2\2HI\3\2\2\2IJ\3\2\2\2JK\5\16")
        buf.write("\b\2K\r\3\2\2\2LP\5\26\f\2MP\5\20\t\2NP\5\22\n\2OL\3\2")
        buf.write("\2\2OM\3\2\2\2ON\3\2\2\2P\17\3\2\2\2QW\t\6\2\2RS\7\31")
        buf.write("\2\2ST\5\2\2\2TU\7&\2\2UW\3\2\2\2VQ\3\2\2\2VR\3\2\2\2")
        buf.write("W\21\3\2\2\2XY\7\33\2\2YZ\7*\2\2Zc\7\31\2\2[`\5\2\2\2")
        buf.write("\\]\7\t\2\2]_\5\2\2\2^\\\3\2\2\2_b\3\2\2\2`^\3\2\2\2`")
        buf.write("a\3\2\2\2ad\3\2\2\2b`\3\2\2\2c[\3\2\2\2cd\3\2\2\2de\3")
        buf.write("\2\2\2eq\7&\2\2fg\7\33\2\2gl\5\30\r\2hi\7\26\2\2ij\5\2")
        buf.write("\2\2jk\7%\2\2km\3\2\2\2lh\3\2\2\2mn\3\2\2\2nl\3\2\2\2")
        buf.write("no\3\2\2\2oq\3\2\2\2pX\3\2\2\2pf\3\2\2\2q\23\3\2\2\2r")
        buf.write("s\7\13\2\2st\7*\2\2t}\7\31\2\2uz\5\2\2\2vw\7\t\2\2wy\5")
        buf.write("\2\2\2xv\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{~\3\2\2")
        buf.write("\2|z\3\2\2\2}u\3\2\2\2}~\3\2\2\2~\177\3\2\2\2\177\u0087")
        buf.write("\7&\2\2\u0080\u0081\7\13\2\2\u0081\u0087\7*\2\2\u0082")
        buf.write("\u0083\7\26\2\2\u0083\u0084\5\2\2\2\u0084\u0085\7%\2\2")
        buf.write("\u0085\u0087\3\2\2\2\u0086r\3\2\2\2\u0086\u0080\3\2\2")
        buf.write("\2\u0086\u0082\3\2\2\2\u0087\25\3\2\2\2\u0088\u008c\7")
        buf.write("*\2\2\u0089\u008b\5\24\13\2\u008a\u0089\3\2\2\2\u008b")
        buf.write("\u008e\3\2\2\2\u008c\u008a\3\2\2\2\u008c\u008d\3\2\2\2")
        buf.write("\u008d\27\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0090\t\7")
        buf.write("\2\2\u0090\31\3\2\2\2\u0091\u0092\5\30\r\2\u0092\u0097")
        buf.write("\5\34\17\2\u0093\u0094\7\t\2\2\u0094\u0096\5\34\17\2\u0095")
        buf.write("\u0093\3\2\2\2\u0096\u0099\3\2\2\2\u0097\u0095\3\2\2\2")
        buf.write("\u0097\u0098\3\2\2\2\u0098\u009a\3\2\2\2\u0099\u0097\3")
        buf.write("\2\2\2\u009a\u009b\7\'\2\2\u009b\33\3\2\2\2\u009c\u00a0")
        buf.write("\7*\2\2\u009d\u009f\5\36\20\2\u009e\u009d\3\2\2\2\u009f")
        buf.write("\u00a2\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2")
        buf.write("\u00a1\35\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a3\u00a4\7\26")
        buf.write("\2\2\u00a4\u00a5\7%\2\2\u00a5\37\3\2\2\2\u00a6\u00af\7")
        buf.write("\31\2\2\u00a7\u00ac\5$\23\2\u00a8\u00a9\7\t\2\2\u00a9")
        buf.write("\u00ab\5$\23\2\u00aa\u00a8\3\2\2\2\u00ab\u00ae\3\2\2\2")
        buf.write("\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00b0\3")
        buf.write("\2\2\2\u00ae\u00ac\3\2\2\2\u00af\u00a7\3\2\2\2\u00af\u00b0")
        buf.write("\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\7&\2\2\u00b2")
        buf.write("\u00b3\5,\27\2\u00b3!\3\2\2\2\u00b4\u00b5\5\30\r\2\u00b5")
        buf.write("\u00b6\7*\2\2\u00b6\u00b7\5 \21\2\u00b7#\3\2\2\2\u00b8")
        buf.write("\u00b9\5\30\r\2\u00b9\u00ba\7*\2\2\u00ba%\3\2\2\2\u00bb")
        buf.write("\u00bc\7\n\2\2\u00bc\u00bd\5 \21\2\u00bd\'\3\2\2\2\u00be")
        buf.write("\u00bf\7\b\2\2\u00bf\u00c2\7*\2\2\u00c0\u00c1\7\17\2\2")
        buf.write("\u00c1\u00c3\7*\2\2\u00c2\u00c0\3\2\2\2\u00c2\u00c3\3")
        buf.write("\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c8\7\25\2\2\u00c5")
        buf.write("\u00c7\5(\25\2\u00c6\u00c5\3\2\2\2\u00c7\u00ca\3\2\2\2")
        buf.write("\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ce\3")
        buf.write("\2\2\2\u00ca\u00c8\3\2\2\2\u00cb\u00cd\5\32\16\2\u00cc")
        buf.write("\u00cb\3\2\2\2\u00cd\u00d0\3\2\2\2\u00ce\u00cc\3\2\2\2")
        buf.write("\u00ce\u00cf\3\2\2\2\u00cf\u00d4\3\2\2\2\u00d0\u00ce\3")
        buf.write("\2\2\2\u00d1\u00d3\5&\24\2\u00d2\u00d1\3\2\2\2\u00d3\u00d6")
        buf.write("\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5")
        buf.write("\u00da\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00d9\5\"\22")
        buf.write("\2\u00d8\u00d7\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8")
        buf.write("\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc")
        buf.write("\u00da\3\2\2\2\u00dd\u00de\7$\2\2\u00de)\3\2\2\2\u00df")
        buf.write("\u00e0\5\26\f\2\u00e0\u00e1\7\r\2\2\u00e1\u00e2\5\2\2")
        buf.write("\2\u00e2+\3\2\2\2\u00e3\u00e6\5\32\16\2\u00e4\u00e6\5")
        buf.write("(\25\2\u00e5\u00e3\3\2\2\2\u00e5\u00e4\3\2\2\2\u00e6\u0124")
        buf.write("\3\2\2\2\u00e7\u00e8\7\23\2\2\u00e8\u00e9\7\31\2\2\u00e9")
        buf.write("\u00ea\5\2\2\2\u00ea\u00eb\7&\2\2\u00eb\u00ee\5,\27\2")
        buf.write("\u00ec\u00ed\7\f\2\2\u00ed\u00ef\5,\27\2\u00ee\u00ec\3")
        buf.write("\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u0124\3\2\2\2\u00f0\u00f1")
        buf.write("\7\20\2\2\u00f1\u00f3\7\31\2\2\u00f2\u00f4\5*\26\2\u00f3")
        buf.write("\u00f2\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2")
        buf.write("\u00f5\u00f7\7\'\2\2\u00f6\u00f8\5\2\2\2\u00f7\u00f6\3")
        buf.write("\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fb")
        buf.write("\7\'\2\2\u00fa\u00fc\5*\26\2\u00fb\u00fa\3\2\2\2\u00fb")
        buf.write("\u00fc\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00fe\7&\2\2")
        buf.write("\u00fe\u0124\5,\27\2\u00ff\u0101\7!\2\2\u0100\u0102\5")
        buf.write("\2\2\2\u0101\u0100\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0103")
        buf.write("\3\2\2\2\u0103\u0124\7\'\2\2\u0104\u0105\7\7\2\2\u0105")
        buf.write("\u0124\7\'\2\2\u0106\u0107\7\37\2\2\u0107\u0108\5\2\2")
        buf.write("\2\u0108\u0109\7\'\2\2\u0109\u0124\3\2\2\2\u010a\u010b")
        buf.write("\7 \2\2\u010b\u010c\5\26\f\2\u010c\u010d\7\'\2\2\u010d")
        buf.write("\u0124\3\2\2\2\u010e\u0112\7\25\2\2\u010f\u0111\5,\27")
        buf.write("\2\u0110\u010f\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110")
        buf.write("\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0115\3\2\2\2\u0114")
        buf.write("\u0112\3\2\2\2\u0115\u0124\7$\2\2\u0116\u0117\7\"\2\2")
        buf.write("\u0117\u011b\7\31\2\2\u0118\u011a\5\2\2\2\u0119\u0118")
        buf.write("\3\2\2\2\u011a\u011d\3\2\2\2\u011b\u0119\3\2\2\2\u011b")
        buf.write("\u011c\3\2\2\2\u011c\u011e\3\2\2\2\u011d\u011b\3\2\2\2")
        buf.write("\u011e\u0124\7&\2\2\u011f\u0120\5*\26\2\u0120\u0121\7")
        buf.write("\'\2\2\u0121\u0124\3\2\2\2\u0122\u0124\7\'\2\2\u0123\u00e5")
        buf.write("\3\2\2\2\u0123\u00e7\3\2\2\2\u0123\u00f0\3\2\2\2\u0123")
        buf.write("\u00ff\3\2\2\2\u0123\u0104\3\2\2\2\u0123\u0106\3\2\2\2")
        buf.write("\u0123\u010a\3\2\2\2\u0123\u010e\3\2\2\2\u0123\u0116\3")
        buf.write("\2\2\2\u0123\u011f\3\2\2\2\u0123\u0122\3\2\2\2\u0124-")
        buf.write("\3\2\2\2\u0125\u0127\5(\25\2\u0126\u0125\3\2\2\2\u0127")
        buf.write("\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2")
        buf.write("\u0129\u012b\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012c\7")
        buf.write("\2\2\3\u012c/\3\2\2\2$\66;@EHOV`cnpz}\u0086\u008c\u0097")
        buf.write("\u00a0\u00ac\u00af\u00c2\u00c8\u00ce\u00d4\u00da\u00e5")
        buf.write("\u00ee\u00f3\u00f7\u00fb\u0101\u0112\u011b\u0123\u0128")
        return buf.getvalue()


class XPPLLParser ( Parser ):

    grammarFileName = "XPPLL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'!='", "'break'", "'class'", "','", "'constructor'", 
                     "'.'", "'else'", "'='", "'=='", "'extends'", "'for'", 
                     "'>'", "'>='", "'if'", "'int'", "'{'", "'['", "'<'", 
                     "'<='", "'('", "'-'", "'new'", "'null'", "'%'", "'+'", 
                     "'print'", "'read'", "'return'", "'super'", "'string'", 
                     "'}'", "']'", "')'", "';'", "'/'", "'*'" ]

    symbolicNames = [ "<INVALID>", "INT_L", "STR_L", "WS", "BANG_EQUAL", 
                      "BREAK", "CLASS", "COMMA", "CTOR", "DOT", "ELSE", 
                      "EQUAL", "EQUAL_EQUAL", "EXTENDS", "FOR", "GREATER", 
                      "GREATER_EQUAL", "IF", "INT", "LBRACE", "LBRACKET", 
                      "LESS", "LESS_EQUAL", "LPARENS", "MINUS", "NEW", "NULL", 
                      "PERCENT", "PLUS", "PRINT", "READ", "RETURN", "SUPER", 
                      "STRING", "RBRACE", "RBRACKET", "RPARENS", "SEMICOLON", 
                      "SLASH", "STAR", "ID" ]

    RULE_expr = 0
    RULE_comp = 1
    RULE_equality = 2
    RULE_add = 3
    RULE_mul = 4
    RULE_unary = 5
    RULE_primary = 6
    RULE_factor = 7
    RULE_alloc = 8
    RULE_access = 9
    RULE_lvalue = 10
    RULE_type_name = 11
    RULE_var_decl = 12
    RULE_name_decl = 13
    RULE_empty_brackets = 14
    RULE_meth_body = 15
    RULE_meth_decl = 16
    RULE_param = 17
    RULE_ctor_decl = 18
    RULE_cls_decl = 19
    RULE_assign = 20
    RULE_stmt = 21
    RULE_program = 22

    ruleNames =  [ "expr", "comp", "equality", "add", "mul", "unary", "primary", 
                   "factor", "alloc", "access", "lvalue", "type_name", "var_decl", 
                   "name_decl", "empty_brackets", "meth_body", "meth_decl", 
                   "param", "ctor_decl", "cls_decl", "assign", "stmt", "program" ]

    EOF = Token.EOF
    INT_L=1
    STR_L=2
    WS=3
    BANG_EQUAL=4
    BREAK=5
    CLASS=6
    COMMA=7
    CTOR=8
    DOT=9
    ELSE=10
    EQUAL=11
    EQUAL_EQUAL=12
    EXTENDS=13
    FOR=14
    GREATER=15
    GREATER_EQUAL=16
    IF=17
    INT=18
    LBRACE=19
    LBRACKET=20
    LESS=21
    LESS_EQUAL=22
    LPARENS=23
    MINUS=24
    NEW=25
    NULL=26
    PERCENT=27
    PLUS=28
    PRINT=29
    READ=30
    RETURN=31
    SUPER=32
    STRING=33
    RBRACE=34
    RBRACKET=35
    RPARENS=36
    SEMICOLON=37
    SLASH=38
    STAR=39
    ID=40

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equality(self):
            return self.getTypedRuleContext(XPPLLParser.EqualityContext,0)


        def comp(self):
            return self.getTypedRuleContext(XPPLLParser.CompContext,0)


        def getRuleIndex(self):
            return XPPLLParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)




    def expr(self):

        localctx = XPPLLParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 46
            self.equality()
            self.state = 47
            self.comp()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CompContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equality(self):
            return self.getTypedRuleContext(XPPLLParser.EqualityContext,0)


        def LESS(self):
            return self.getToken(XPPLLParser.LESS, 0)

        def LESS_EQUAL(self):
            return self.getToken(XPPLLParser.LESS_EQUAL, 0)

        def GREATER(self):
            return self.getToken(XPPLLParser.GREATER, 0)

        def GREATER_EQUAL(self):
            return self.getToken(XPPLLParser.GREATER_EQUAL, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_comp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp" ):
                listener.enterComp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp" ):
                listener.exitComp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp" ):
                return visitor.visitComp(self)
            else:
                return visitor.visitChildren(self)




    def comp(self):

        localctx = XPPLLParser.CompContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_comp)
        self._la = 0 # Token type
        try:
            self.state = 52
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLLParser.GREATER, XPPLLParser.GREATER_EQUAL, XPPLLParser.LESS, XPPLLParser.LESS_EQUAL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 49
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.GREATER) | (1 << XPPLLParser.GREATER_EQUAL) | (1 << XPPLLParser.LESS) | (1 << XPPLLParser.LESS_EQUAL))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 50
                self.equality()
                pass
            elif token in [XPPLLParser.INT_L, XPPLLParser.STR_L, XPPLLParser.COMMA, XPPLLParser.LPARENS, XPPLLParser.MINUS, XPPLLParser.NEW, XPPLLParser.NULL, XPPLLParser.PLUS, XPPLLParser.RBRACKET, XPPLLParser.RPARENS, XPPLLParser.SEMICOLON, XPPLLParser.ID]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualityContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # AddContext
            self.op = None # Token
            self.rhs = None # AddContext

        def add(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.AddContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.AddContext,i)


        def EQUAL_EQUAL(self):
            return self.getToken(XPPLLParser.EQUAL_EQUAL, 0)

        def BANG_EQUAL(self):
            return self.getToken(XPPLLParser.BANG_EQUAL, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_equality

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality" ):
                listener.enterEquality(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality" ):
                listener.exitEquality(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquality" ):
                return visitor.visitEquality(self)
            else:
                return visitor.visitChildren(self)




    def equality(self):

        localctx = XPPLLParser.EqualityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_equality)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            localctx.lhs = self.add()
            self.state = 57
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLLParser.BANG_EQUAL or _la==XPPLLParser.EQUAL_EQUAL:
                self.state = 55
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLLParser.BANG_EQUAL or _la==XPPLLParser.EQUAL_EQUAL):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 56
                localctx.rhs = self.add()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AddContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # MulContext
            self.op = None # Token
            self.rhs = None # MulContext

        def mul(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.MulContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.MulContext,i)


        def PLUS(self):
            return self.getToken(XPPLLParser.PLUS, 0)

        def MINUS(self):
            return self.getToken(XPPLLParser.MINUS, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_add

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAdd" ):
                listener.enterAdd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAdd" ):
                listener.exitAdd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAdd" ):
                return visitor.visitAdd(self)
            else:
                return visitor.visitChildren(self)




    def add(self):

        localctx = XPPLLParser.AddContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_add)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 59
            localctx.lhs = self.mul()
            self.state = 62
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 60
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLLParser.MINUS or _la==XPPLLParser.PLUS):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 61
                localctx.rhs = self.mul()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MulContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # UnaryContext
            self.op = None # Token
            self.rhs = None # UnaryContext

        def unary(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.UnaryContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.UnaryContext,i)


        def STAR(self):
            return self.getToken(XPPLLParser.STAR, 0)

        def SLASH(self):
            return self.getToken(XPPLLParser.SLASH, 0)

        def PERCENT(self):
            return self.getToken(XPPLLParser.PERCENT, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_mul

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMul" ):
                listener.enterMul(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMul" ):
                listener.exitMul(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMul" ):
                return visitor.visitMul(self)
            else:
                return visitor.visitChildren(self)




    def mul(self):

        localctx = XPPLLParser.MulContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_mul)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            localctx.lhs = self.unary()
            self.state = 67
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.PERCENT) | (1 << XPPLLParser.SLASH) | (1 << XPPLLParser.STAR))) != 0):
                self.state = 65
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.PERCENT) | (1 << XPPLLParser.SLASH) | (1 << XPPLLParser.STAR))) != 0)):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 66
                localctx.rhs = self.unary()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.op = None # Token
            self.rhs = None # PrimaryContext

        def primary(self):
            return self.getTypedRuleContext(XPPLLParser.PrimaryContext,0)


        def PLUS(self):
            return self.getToken(XPPLLParser.PLUS, 0)

        def MINUS(self):
            return self.getToken(XPPLLParser.MINUS, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_unary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnary" ):
                listener.enterUnary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnary" ):
                listener.exitUnary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnary" ):
                return visitor.visitUnary(self)
            else:
                return visitor.visitChildren(self)




    def unary(self):

        localctx = XPPLLParser.UnaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_unary)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLLParser.MINUS or _la==XPPLLParser.PLUS:
                self.state = 69
                localctx.op = self._input.LT(1)
                _la = self._input.LA(1)
                if not(_la==XPPLLParser.MINUS or _la==XPPLLParser.PLUS):
                    localctx.op = self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()


            self.state = 72
            localctx.rhs = self.primary()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimaryContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lvalue(self):
            return self.getTypedRuleContext(XPPLLParser.LvalueContext,0)


        def factor(self):
            return self.getTypedRuleContext(XPPLLParser.FactorContext,0)


        def alloc(self):
            return self.getTypedRuleContext(XPPLLParser.AllocContext,0)


        def getRuleIndex(self):
            return XPPLLParser.RULE_primary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimary" ):
                listener.enterPrimary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimary" ):
                listener.exitPrimary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimary" ):
                return visitor.visitPrimary(self)
            else:
                return visitor.visitChildren(self)




    def primary(self):

        localctx = XPPLLParser.PrimaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_primary)
        try:
            self.state = 77
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLLParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 74
                self.lvalue()
                pass
            elif token in [XPPLLParser.INT_L, XPPLLParser.STR_L, XPPLLParser.LPARENS, XPPLLParser.NULL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 75
                self.factor()
                pass
            elif token in [XPPLLParser.NEW]:
                self.enterOuterAlt(localctx, 3)
                self.state = 76
                self.alloc()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FactorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLLParser.RULE_factor

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class LitContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT_L(self):
            return self.getToken(XPPLLParser.INT_L, 0)
        def STR_L(self):
            return self.getToken(XPPLLParser.STR_L, 0)
        def NULL(self):
            return self.getToken(XPPLLParser.NULL, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLit" ):
                listener.enterLit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLit" ):
                listener.exitLit(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLit" ):
                return visitor.visitLit(self)
            else:
                return visitor.visitChildren(self)


    class GroupContext(FactorContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.FactorContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)

        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGroup" ):
                listener.enterGroup(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGroup" ):
                listener.exitGroup(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGroup" ):
                return visitor.visitGroup(self)
            else:
                return visitor.visitChildren(self)



    def factor(self):

        localctx = XPPLLParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_factor)
        self._la = 0 # Token type
        try:
            self.state = 84
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [XPPLLParser.INT_L, XPPLLParser.STR_L, XPPLLParser.NULL]:
                localctx = XPPLLParser.LitContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 79
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.NULL))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass
            elif token in [XPPLLParser.LPARENS]:
                localctx = XPPLLParser.GroupContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                self.match(XPPLLParser.LPARENS)
                self.state = 81
                self.expr()
                self.state = 82
                self.match(XPPLLParser.RPARENS)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AllocContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLLParser.RULE_alloc

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Array_allocContext(AllocContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.AllocContext
            super().__init__(parser)
            self.typ_ = None # Type_nameContext
            self._expr = None # ExprContext
            self.dims = list() # of ExprContexts
            self.copyFrom(ctx)

        def NEW(self):
            return self.getToken(XPPLLParser.NEW, 0)
        def type_name(self):
            return self.getTypedRuleContext(XPPLLParser.Type_nameContext,0)

        def LBRACKET(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.LBRACKET)
            else:
                return self.getToken(XPPLLParser.LBRACKET, i)
        def RBRACKET(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.RBRACKET)
            else:
                return self.getToken(XPPLLParser.RBRACKET, i)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.ExprContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray_alloc" ):
                listener.enterArray_alloc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray_alloc" ):
                listener.exitArray_alloc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArray_alloc" ):
                return visitor.visitArray_alloc(self)
            else:
                return visitor.visitChildren(self)


    class Ctor_callContext(AllocContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.AllocContext
            super().__init__(parser)
            self.name = None # Token
            self._expr = None # ExprContext
            self.args = list() # of ExprContexts
            self.copyFrom(ctx)

        def NEW(self):
            return self.getToken(XPPLLParser.NEW, 0)
        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)
        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.ExprContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.COMMA)
            else:
                return self.getToken(XPPLLParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCtor_call" ):
                listener.enterCtor_call(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCtor_call" ):
                listener.exitCtor_call(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCtor_call" ):
                return visitor.visitCtor_call(self)
            else:
                return visitor.visitChildren(self)



    def alloc(self):

        localctx = XPPLLParser.AllocContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_alloc)
        self._la = 0 # Token type
        try:
            self.state = 110
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                localctx = XPPLLParser.Ctor_callContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 86
                self.match(XPPLLParser.NEW)
                self.state = 87
                localctx.name = self.match(XPPLLParser.ID)
                self.state = 88
                self.match(XPPLLParser.LPARENS)
                self.state = 97
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.LPARENS) | (1 << XPPLLParser.MINUS) | (1 << XPPLLParser.NEW) | (1 << XPPLLParser.NULL) | (1 << XPPLLParser.PLUS) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 89
                    localctx._expr = self.expr()
                    localctx.args.append(localctx._expr)
                    self.state = 94
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==XPPLLParser.COMMA:
                        self.state = 90
                        self.match(XPPLLParser.COMMA)
                        self.state = 91
                        localctx._expr = self.expr()
                        localctx.args.append(localctx._expr)
                        self.state = 96
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 99
                self.match(XPPLLParser.RPARENS)
                pass

            elif la_ == 2:
                localctx = XPPLLParser.Array_allocContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 100
                self.match(XPPLLParser.NEW)
                self.state = 101
                localctx.typ_ = self.type_name()
                self.state = 106 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 102
                    self.match(XPPLLParser.LBRACKET)
                    self.state = 103
                    localctx._expr = self.expr()
                    localctx.dims.append(localctx._expr)
                    self.state = 104
                    self.match(XPPLLParser.RBRACKET)
                    self.state = 108 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==XPPLLParser.LBRACKET):
                        break

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AccessContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLLParser.RULE_access

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Attr_getContext(AccessContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.AccessContext
            super().__init__(parser)
            self.name = None # Token
            self.copyFrom(ctx)

        def DOT(self):
            return self.getToken(XPPLLParser.DOT, 0)
        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAttr_get" ):
                listener.enterAttr_get(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAttr_get" ):
                listener.exitAttr_get(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAttr_get" ):
                return visitor.visitAttr_get(self)
            else:
                return visitor.visitChildren(self)


    class Method_callContext(AccessContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.AccessContext
            super().__init__(parser)
            self.name = None # Token
            self._expr = None # ExprContext
            self.args = list() # of ExprContexts
            self.copyFrom(ctx)

        def DOT(self):
            return self.getToken(XPPLLParser.DOT, 0)
        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)
        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.ExprContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.ExprContext,i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.COMMA)
            else:
                return self.getToken(XPPLLParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMethod_call" ):
                listener.enterMethod_call(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMethod_call" ):
                listener.exitMethod_call(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_call" ):
                return visitor.visitMethod_call(self)
            else:
                return visitor.visitChildren(self)


    class Item_getContext(AccessContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.AccessContext
            super().__init__(parser)
            self.value = None # ExprContext
            self.copyFrom(ctx)

        def LBRACKET(self):
            return self.getToken(XPPLLParser.LBRACKET, 0)
        def RBRACKET(self):
            return self.getToken(XPPLLParser.RBRACKET, 0)
        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterItem_get" ):
                listener.enterItem_get(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitItem_get" ):
                listener.exitItem_get(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitItem_get" ):
                return visitor.visitItem_get(self)
            else:
                return visitor.visitChildren(self)



    def access(self):

        localctx = XPPLLParser.AccessContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_access)
        self._la = 0 # Token type
        try:
            self.state = 132
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
            if la_ == 1:
                localctx = XPPLLParser.Method_callContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 112
                self.match(XPPLLParser.DOT)
                self.state = 113
                localctx.name = self.match(XPPLLParser.ID)
                self.state = 114
                self.match(XPPLLParser.LPARENS)
                self.state = 123
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.LPARENS) | (1 << XPPLLParser.MINUS) | (1 << XPPLLParser.NEW) | (1 << XPPLLParser.NULL) | (1 << XPPLLParser.PLUS) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 115
                    localctx._expr = self.expr()
                    localctx.args.append(localctx._expr)
                    self.state = 120
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==XPPLLParser.COMMA:
                        self.state = 116
                        self.match(XPPLLParser.COMMA)
                        self.state = 117
                        localctx._expr = self.expr()
                        localctx.args.append(localctx._expr)
                        self.state = 122
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                self.state = 125
                self.match(XPPLLParser.RPARENS)
                pass

            elif la_ == 2:
                localctx = XPPLLParser.Attr_getContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 126
                self.match(XPPLLParser.DOT)
                self.state = 127
                localctx.name = self.match(XPPLLParser.ID)
                pass

            elif la_ == 3:
                localctx = XPPLLParser.Item_getContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 128
                self.match(XPPLLParser.LBRACKET)
                self.state = 129
                localctx.value = self.expr()
                self.state = 130
                self.match(XPPLLParser.RBRACKET)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LvalueContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token

        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def access(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.AccessContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.AccessContext,i)


        def getRuleIndex(self):
            return XPPLLParser.RULE_lvalue

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLvalue" ):
                listener.enterLvalue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLvalue" ):
                listener.exitLvalue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLvalue" ):
                return visitor.visitLvalue(self)
            else:
                return visitor.visitChildren(self)




    def lvalue(self):

        localctx = XPPLLParser.LvalueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_lvalue)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 134
            localctx.name = self.match(XPPLLParser.ID)
            self.state = 138
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.DOT or _la==XPPLLParser.LBRACKET:
                self.state = 135
                self.access()
                self.state = 140
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def STRING(self):
            return self.getToken(XPPLLParser.STRING, 0)

        def INT(self):
            return self.getToken(XPPLLParser.INT, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_type_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_name" ):
                listener.enterType_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_name" ):
                listener.exitType_name(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_name" ):
                return visitor.visitType_name(self)
            else:
                return visitor.visitChildren(self)




    def type_name(self):

        localctx = XPPLLParser.Type_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_type_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT) | (1 << XPPLLParser.STRING) | (1 << XPPLLParser.ID))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Type_nameContext
            self._name_decl = None # Name_declContext
            self.variables = list() # of Name_declContexts

        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)

        def type_name(self):
            return self.getTypedRuleContext(XPPLLParser.Type_nameContext,0)


        def name_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Name_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Name_declContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.COMMA)
            else:
                return self.getToken(XPPLLParser.COMMA, i)

        def getRuleIndex(self):
            return XPPLLParser.RULE_var_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar_decl" ):
                listener.enterVar_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar_decl" ):
                listener.exitVar_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_decl" ):
                return visitor.visitVar_decl(self)
            else:
                return visitor.visitChildren(self)




    def var_decl(self):

        localctx = XPPLLParser.Var_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_var_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 143
            localctx.type_ = self.type_name()
            self.state = 144
            localctx._name_decl = self.name_decl()
            localctx.variables.append(localctx._name_decl)
            self.state = 149
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.COMMA:
                self.state = 145
                self.match(XPPLLParser.COMMA)
                self.state = 146
                localctx._name_decl = self.name_decl()
                localctx.variables.append(localctx._name_decl)
                self.state = 151
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 152
            self.match(XPPLLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Name_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self._empty_brackets = None # Empty_bracketsContext
            self.dims = list() # of Empty_bracketsContexts

        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def empty_brackets(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Empty_bracketsContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Empty_bracketsContext,i)


        def getRuleIndex(self):
            return XPPLLParser.RULE_name_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName_decl" ):
                listener.enterName_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName_decl" ):
                listener.exitName_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitName_decl" ):
                return visitor.visitName_decl(self)
            else:
                return visitor.visitChildren(self)




    def name_decl(self):

        localctx = XPPLLParser.Name_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_name_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            localctx.name = self.match(XPPLLParser.ID)
            self.state = 158
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.LBRACKET:
                self.state = 155
                localctx._empty_brackets = self.empty_brackets()
                localctx.dims.append(localctx._empty_brackets)
                self.state = 160
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Empty_bracketsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACKET(self):
            return self.getToken(XPPLLParser.LBRACKET, 0)

        def RBRACKET(self):
            return self.getToken(XPPLLParser.RBRACKET, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_empty_brackets

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmpty_brackets" ):
                listener.enterEmpty_brackets(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmpty_brackets" ):
                listener.exitEmpty_brackets(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEmpty_brackets" ):
                return visitor.visitEmpty_brackets(self)
            else:
                return visitor.visitChildren(self)




    def empty_brackets(self):

        localctx = XPPLLParser.Empty_bracketsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_empty_brackets)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 161
            self.match(XPPLLParser.LBRACKET)
            self.state = 162
            self.match(XPPLLParser.RBRACKET)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meth_bodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._param = None # ParamContext
            self.params = list() # of ParamContexts
            self.body = None # StmtContext

        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)

        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)

        def stmt(self):
            return self.getTypedRuleContext(XPPLLParser.StmtContext,0)


        def param(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.ParamContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.ParamContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.COMMA)
            else:
                return self.getToken(XPPLLParser.COMMA, i)

        def getRuleIndex(self):
            return XPPLLParser.RULE_meth_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeth_body" ):
                listener.enterMeth_body(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeth_body" ):
                listener.exitMeth_body(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeth_body" ):
                return visitor.visitMeth_body(self)
            else:
                return visitor.visitChildren(self)




    def meth_body(self):

        localctx = XPPLLParser.Meth_bodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_meth_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 164
            self.match(XPPLLParser.LPARENS)
            self.state = 173
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT) | (1 << XPPLLParser.STRING) | (1 << XPPLLParser.ID))) != 0):
                self.state = 165
                localctx._param = self.param()
                localctx.params.append(localctx._param)
                self.state = 170
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==XPPLLParser.COMMA:
                    self.state = 166
                    self.match(XPPLLParser.COMMA)
                    self.state = 167
                    localctx._param = self.param()
                    localctx.params.append(localctx._param)
                    self.state = 172
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 175
            self.match(XPPLLParser.RPARENS)
            self.state = 176
            localctx.body = self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Meth_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ret_type = None # Type_nameContext
            self.name = None # Token
            self.body = None # Meth_bodyContext

        def type_name(self):
            return self.getTypedRuleContext(XPPLLParser.Type_nameContext,0)


        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def meth_body(self):
            return self.getTypedRuleContext(XPPLLParser.Meth_bodyContext,0)


        def getRuleIndex(self):
            return XPPLLParser.RULE_meth_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMeth_decl" ):
                listener.enterMeth_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMeth_decl" ):
                listener.exitMeth_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMeth_decl" ):
                return visitor.visitMeth_decl(self)
            else:
                return visitor.visitChildren(self)




    def meth_decl(self):

        localctx = XPPLLParser.Meth_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_meth_decl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            localctx.ret_type = self.type_name()
            self.state = 179
            localctx.name = self.match(XPPLLParser.ID)
            self.state = 180
            localctx.body = self.meth_body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Type_nameContext
            self.name = None # Token

        def type_name(self):
            return self.getTypedRuleContext(XPPLLParser.Type_nameContext,0)


        def ID(self):
            return self.getToken(XPPLLParser.ID, 0)

        def getRuleIndex(self):
            return XPPLLParser.RULE_param

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParam" ):
                listener.enterParam(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParam" ):
                listener.exitParam(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParam" ):
                return visitor.visitParam(self)
            else:
                return visitor.visitChildren(self)




    def param(self):

        localctx = XPPLLParser.ParamContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_param)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 182
            localctx.type_ = self.type_name()
            self.state = 183
            localctx.name = self.match(XPPLLParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ctor_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.body = None # Meth_bodyContext

        def CTOR(self):
            return self.getToken(XPPLLParser.CTOR, 0)

        def meth_body(self):
            return self.getTypedRuleContext(XPPLLParser.Meth_bodyContext,0)


        def getRuleIndex(self):
            return XPPLLParser.RULE_ctor_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCtor_decl" ):
                listener.enterCtor_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCtor_decl" ):
                listener.exitCtor_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCtor_decl" ):
                return visitor.visitCtor_decl(self)
            else:
                return visitor.visitChildren(self)




    def ctor_decl(self):

        localctx = XPPLLParser.Ctor_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_ctor_decl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 185
            self.match(XPPLLParser.CTOR)
            self.state = 186
            localctx.body = self.meth_body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Cls_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.name = None # Token
            self.super_class = None # Token
            self._cls_decl = None # Cls_declContext
            self.nested = list() # of Cls_declContexts
            self._var_decl = None # Var_declContext
            self.variables = list() # of Var_declContexts
            self._ctor_decl = None # Ctor_declContext
            self.ctors = list() # of Ctor_declContexts
            self._meth_decl = None # Meth_declContext
            self.meths = list() # of Meth_declContexts

        def CLASS(self):
            return self.getToken(XPPLLParser.CLASS, 0)

        def LBRACE(self):
            return self.getToken(XPPLLParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(XPPLLParser.RBRACE, 0)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.ID)
            else:
                return self.getToken(XPPLLParser.ID, i)

        def EXTENDS(self):
            return self.getToken(XPPLLParser.EXTENDS, 0)

        def cls_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Cls_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Cls_declContext,i)


        def var_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Var_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Var_declContext,i)


        def ctor_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Ctor_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Ctor_declContext,i)


        def meth_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Meth_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Meth_declContext,i)


        def getRuleIndex(self):
            return XPPLLParser.RULE_cls_decl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCls_decl" ):
                listener.enterCls_decl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCls_decl" ):
                listener.exitCls_decl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCls_decl" ):
                return visitor.visitCls_decl(self)
            else:
                return visitor.visitChildren(self)




    def cls_decl(self):

        localctx = XPPLLParser.Cls_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_cls_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 188
            self.match(XPPLLParser.CLASS)
            self.state = 189
            localctx.name = self.match(XPPLLParser.ID)
            self.state = 192
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==XPPLLParser.EXTENDS:
                self.state = 190
                self.match(XPPLLParser.EXTENDS)
                self.state = 191
                localctx.super_class = self.match(XPPLLParser.ID)


            self.state = 194
            self.match(XPPLLParser.LBRACE)
            self.state = 198
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.CLASS:
                self.state = 195
                localctx._cls_decl = self.cls_decl()
                localctx.nested.append(localctx._cls_decl)
                self.state = 200
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 204
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,21,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 201
                    localctx._var_decl = self.var_decl()
                    localctx.variables.append(localctx._var_decl) 
                self.state = 206
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,21,self._ctx)

            self.state = 210
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.CTOR:
                self.state = 207
                localctx._ctor_decl = self.ctor_decl()
                localctx.ctors.append(localctx._ctor_decl)
                self.state = 212
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT) | (1 << XPPLLParser.STRING) | (1 << XPPLLParser.ID))) != 0):
                self.state = 213
                localctx._meth_decl = self.meth_decl()
                localctx.meths.append(localctx._meth_decl)
                self.state = 218
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 219
            self.match(XPPLLParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.lhs = None # LvalueContext
            self.rhs = None # ExprContext

        def EQUAL(self):
            return self.getToken(XPPLLParser.EQUAL, 0)

        def lvalue(self):
            return self.getTypedRuleContext(XPPLLParser.LvalueContext,0)


        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)


        def getRuleIndex(self):
            return XPPLLParser.RULE_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign" ):
                listener.enterAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign" ):
                listener.exitAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)




    def assign(self):

        localctx = XPPLLParser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 221
            localctx.lhs = self.lvalue()
            self.state = 222
            self.match(XPPLLParser.EQUAL)
            self.state = 223
            localctx.rhs = self.expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return XPPLLParser.RULE_stmt

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class Read_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.des = None # LvalueContext
            self.copyFrom(ctx)

        def READ(self):
            return self.getToken(XPPLLParser.READ, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)
        def lvalue(self):
            return self.getTypedRuleContext(XPPLLParser.LvalueContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRead_stmt" ):
                listener.enterRead_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRead_stmt" ):
                listener.exitRead_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRead_stmt" ):
                return visitor.visitRead_stmt(self)
            else:
                return visitor.visitChildren(self)


    class DeclContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def var_decl(self):
            return self.getTypedRuleContext(XPPLLParser.Var_declContext,0)

        def cls_decl(self):
            return self.getTypedRuleContext(XPPLLParser.Cls_declContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDecl" ):
                listener.enterDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDecl" ):
                listener.exitDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl" ):
                return visitor.visitDecl(self)
            else:
                return visitor.visitChildren(self)


    class Block_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self._stmt = None # StmtContext
            self.stmts = list() # of StmtContexts
            self.copyFrom(ctx)

        def LBRACE(self):
            return self.getToken(XPPLLParser.LBRACE, 0)
        def RBRACE(self):
            return self.getToken(XPPLLParser.RBRACE, 0)
        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.StmtContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.StmtContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlock_stmt" ):
                listener.enterBlock_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlock_stmt" ):
                listener.exitBlock_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock_stmt" ):
                return visitor.visitBlock_stmt(self)
            else:
                return visitor.visitChildren(self)


    class If_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.condition = None # ExprContext
            self.then_branch = None # StmtContext
            self.else_branch = None # StmtContext
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(XPPLLParser.IF, 0)
        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)
        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.StmtContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.StmtContext,i)

        def ELSE(self):
            return self.getToken(XPPLLParser.ELSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_stmt" ):
                listener.enterIf_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_stmt" ):
                listener.exitIf_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIf_stmt" ):
                return visitor.visitIf_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Break_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def BREAK(self):
            return self.getToken(XPPLLParser.BREAK, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBreak_stmt" ):
                listener.enterBreak_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBreak_stmt" ):
                listener.exitBreak_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreak_stmt" ):
                return visitor.visitBreak_stmt(self)
            else:
                return visitor.visitChildren(self)


    class For_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.assignment = None # AssignContext
            self.condition = None # ExprContext
            self.step = None # AssignContext
            self.body = None # StmtContext
            self.copyFrom(ctx)

        def FOR(self):
            return self.getToken(XPPLLParser.FOR, 0)
        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(XPPLLParser.SEMICOLON)
            else:
                return self.getToken(XPPLLParser.SEMICOLON, i)
        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)
        def stmt(self):
            return self.getTypedRuleContext(XPPLLParser.StmtContext,0)

        def assign(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.AssignContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.AssignContext,i)

        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFor_stmt" ):
                listener.enterFor_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFor_stmt" ):
                listener.exitFor_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFor_stmt" ):
                return visitor.visitFor_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Super_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self._expr = None # ExprContext
            self.args = list() # of ExprContexts
            self.copyFrom(ctx)

        def SUPER(self):
            return self.getToken(XPPLLParser.SUPER, 0)
        def LPARENS(self):
            return self.getToken(XPPLLParser.LPARENS, 0)
        def RPARENS(self):
            return self.getToken(XPPLLParser.RPARENS, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.ExprContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSuper_stmt" ):
                listener.enterSuper_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSuper_stmt" ):
                listener.exitSuper_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSuper_stmt" ):
                return visitor.visitSuper_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Print_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.val = None # ExprContext
            self.copyFrom(ctx)

        def PRINT(self):
            return self.getToken(XPPLLParser.PRINT, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)
        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrint_stmt" ):
                listener.enterPrint_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrint_stmt" ):
                listener.exitPrint_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrint_stmt" ):
                return visitor.visitPrint_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Scolon_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterScolon_stmt" ):
                listener.enterScolon_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitScolon_stmt" ):
                listener.exitScolon_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScolon_stmt" ):
                return visitor.visitScolon_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Return_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.val = None # ExprContext
            self.copyFrom(ctx)

        def RETURN(self):
            return self.getToken(XPPLLParser.RETURN, 0)
        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)
        def expr(self):
            return self.getTypedRuleContext(XPPLLParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReturn_stmt" ):
                listener.enterReturn_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReturn_stmt" ):
                listener.exitReturn_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturn_stmt" ):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)


    class Assign_stmtContext(StmtContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a XPPLLParser.StmtContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def assign(self):
            return self.getTypedRuleContext(XPPLLParser.AssignContext,0)

        def SEMICOLON(self):
            return self.getToken(XPPLLParser.SEMICOLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_stmt" ):
                listener.enterAssign_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_stmt" ):
                listener.exitAssign_stmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_stmt" ):
                return visitor.visitAssign_stmt(self)
            else:
                return visitor.visitChildren(self)



    def stmt(self):

        localctx = XPPLLParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_stmt)
        self._la = 0 # Token type
        try:
            self.state = 289
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                localctx = XPPLLParser.DeclContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 227
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [XPPLLParser.INT, XPPLLParser.STRING, XPPLLParser.ID]:
                    self.state = 225
                    self.var_decl()
                    pass
                elif token in [XPPLLParser.CLASS]:
                    self.state = 226
                    self.cls_decl()
                    pass
                else:
                    raise NoViableAltException(self)

                pass

            elif la_ == 2:
                localctx = XPPLLParser.If_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 229
                self.match(XPPLLParser.IF)
                self.state = 230
                self.match(XPPLLParser.LPARENS)
                self.state = 231
                localctx.condition = self.expr()
                self.state = 232
                self.match(XPPLLParser.RPARENS)
                self.state = 233
                localctx.then_branch = self.stmt()
                self.state = 236
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,25,self._ctx)
                if la_ == 1:
                    self.state = 234
                    self.match(XPPLLParser.ELSE)
                    self.state = 235
                    localctx.else_branch = self.stmt()


                pass

            elif la_ == 3:
                localctx = XPPLLParser.For_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 238
                self.match(XPPLLParser.FOR)
                self.state = 239
                self.match(XPPLLParser.LPARENS)
                self.state = 241
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==XPPLLParser.ID:
                    self.state = 240
                    localctx.assignment = self.assign()


                self.state = 243
                self.match(XPPLLParser.SEMICOLON)
                self.state = 245
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.LPARENS) | (1 << XPPLLParser.MINUS) | (1 << XPPLLParser.NEW) | (1 << XPPLLParser.NULL) | (1 << XPPLLParser.PLUS) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 244
                    localctx.condition = self.expr()


                self.state = 247
                self.match(XPPLLParser.SEMICOLON)
                self.state = 249
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==XPPLLParser.ID:
                    self.state = 248
                    localctx.step = self.assign()


                self.state = 251
                self.match(XPPLLParser.RPARENS)
                self.state = 252
                localctx.body = self.stmt()
                pass

            elif la_ == 4:
                localctx = XPPLLParser.Return_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 253
                self.match(XPPLLParser.RETURN)
                self.state = 255
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.LPARENS) | (1 << XPPLLParser.MINUS) | (1 << XPPLLParser.NEW) | (1 << XPPLLParser.NULL) | (1 << XPPLLParser.PLUS) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 254
                    localctx.val = self.expr()


                self.state = 257
                self.match(XPPLLParser.SEMICOLON)
                pass

            elif la_ == 5:
                localctx = XPPLLParser.Break_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 258
                self.match(XPPLLParser.BREAK)
                self.state = 259
                self.match(XPPLLParser.SEMICOLON)
                pass

            elif la_ == 6:
                localctx = XPPLLParser.Print_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 260
                self.match(XPPLLParser.PRINT)
                self.state = 261
                localctx.val = self.expr()
                self.state = 262
                self.match(XPPLLParser.SEMICOLON)
                pass

            elif la_ == 7:
                localctx = XPPLLParser.Read_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 264
                self.match(XPPLLParser.READ)
                self.state = 265
                localctx.des = self.lvalue()
                self.state = 266
                self.match(XPPLLParser.SEMICOLON)
                pass

            elif la_ == 8:
                localctx = XPPLLParser.Block_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 268
                self.match(XPPLLParser.LBRACE)
                self.state = 272
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.BREAK) | (1 << XPPLLParser.CLASS) | (1 << XPPLLParser.FOR) | (1 << XPPLLParser.IF) | (1 << XPPLLParser.INT) | (1 << XPPLLParser.LBRACE) | (1 << XPPLLParser.PRINT) | (1 << XPPLLParser.READ) | (1 << XPPLLParser.RETURN) | (1 << XPPLLParser.SUPER) | (1 << XPPLLParser.STRING) | (1 << XPPLLParser.SEMICOLON) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 269
                    localctx._stmt = self.stmt()
                    localctx.stmts.append(localctx._stmt)
                    self.state = 274
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 275
                self.match(XPPLLParser.RBRACE)
                pass

            elif la_ == 9:
                localctx = XPPLLParser.Super_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 276
                self.match(XPPLLParser.SUPER)
                self.state = 277
                self.match(XPPLLParser.LPARENS)
                self.state = 281
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << XPPLLParser.INT_L) | (1 << XPPLLParser.STR_L) | (1 << XPPLLParser.LPARENS) | (1 << XPPLLParser.MINUS) | (1 << XPPLLParser.NEW) | (1 << XPPLLParser.NULL) | (1 << XPPLLParser.PLUS) | (1 << XPPLLParser.ID))) != 0):
                    self.state = 278
                    localctx._expr = self.expr()
                    localctx.args.append(localctx._expr)
                    self.state = 283
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 284
                self.match(XPPLLParser.RPARENS)
                pass

            elif la_ == 10:
                localctx = XPPLLParser.Assign_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 285
                self.assign()
                self.state = 286
                self.match(XPPLLParser.SEMICOLON)
                pass

            elif la_ == 11:
                localctx = XPPLLParser.Scolon_stmtContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 288
                self.match(XPPLLParser.SEMICOLON)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._cls_decl = None # Cls_declContext
            self.classes = list() # of Cls_declContexts

        def EOF(self):
            return self.getToken(XPPLLParser.EOF, 0)

        def cls_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(XPPLLParser.Cls_declContext)
            else:
                return self.getTypedRuleContext(XPPLLParser.Cls_declContext,i)


        def getRuleIndex(self):
            return XPPLLParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = XPPLLParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 294
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==XPPLLParser.CLASS:
                self.state = 291
                localctx._cls_decl = self.cls_decl()
                localctx.classes.append(localctx._cls_decl)
                self.state = 296
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 297
            self.match(XPPLLParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





