# Generated from /home/henry/Workspace/chab/XPPLL.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .XPPLL import XPPLL
else:
    from XPPLL import XPPLL

# This class defines a complete generic visitor for a parse tree produced by XPPLL.

class XPPLLVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by XPPLL#expr.
    def visitExpr(self, ctx:XPPLL.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#equality.
    def visitEquality(self, ctx:XPPLL.EqualityContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#add.
    def visitAdd(self, ctx:XPPLL.AddContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#mul.
    def visitMul(self, ctx:XPPLL.MulContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#unary.
    def visitUnary(self, ctx:XPPLL.UnaryContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#primary.
    def visitPrimary(self, ctx:XPPLL.PrimaryContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#lit.
    def visitLit(self, ctx:XPPLL.LitContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#group.
    def visitGroup(self, ctx:XPPLL.GroupContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#array_alloc.
    def visitArray_alloc(self, ctx:XPPLL.Array_allocContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#alloc.
    def visitAlloc(self, ctx:XPPLL.AllocContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#access.
    def visitAccess(self, ctx:XPPLL.AccessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#call_access.
    def visitCall_access(self, ctx:XPPLL.Call_accessContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#get_item.
    def visitGet_item(self, ctx:XPPLL.Get_itemContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#get_attr.
    def visitGet_attr(self, ctx:XPPLL.Get_attrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#type_name.
    def visitType_name(self, ctx:XPPLL.Type_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#var_decl.
    def visitVar_decl(self, ctx:XPPLL.Var_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#dec.
    def visitDec(self, ctx:XPPLL.DecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#name_decl.
    def visitName_decl(self, ctx:XPPLL.Name_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#dimension.
    def visitDimension(self, ctx:XPPLL.DimensionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#meth_body.
    def visitMeth_body(self, ctx:XPPLL.Meth_bodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#meth_decl.
    def visitMeth_decl(self, ctx:XPPLL.Meth_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#param.
    def visitParam(self, ctx:XPPLL.ParamContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#ctor_decl.
    def visitCtor_decl(self, ctx:XPPLL.Ctor_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#cls_decl.
    def visitCls_decl(self, ctx:XPPLL.Cls_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#assign.
    def visitAssign(self, ctx:XPPLL.AssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#decl.
    def visitDecl(self, ctx:XPPLL.DeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#if_stmt.
    def visitIf_stmt(self, ctx:XPPLL.If_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#for_stmt.
    def visitFor_stmt(self, ctx:XPPLL.For_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#return_stmt.
    def visitReturn_stmt(self, ctx:XPPLL.Return_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#break_stmt.
    def visitBreak_stmt(self, ctx:XPPLL.Break_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#print_stmt.
    def visitPrint_stmt(self, ctx:XPPLL.Print_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#read_stmt.
    def visitRead_stmt(self, ctx:XPPLL.Read_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#block_stmt.
    def visitBlock_stmt(self, ctx:XPPLL.Block_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#super_stmt.
    def visitSuper_stmt(self, ctx:XPPLL.Super_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#assign_stmt.
    def visitAssign_stmt(self, ctx:XPPLL.Assign_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#scolon_stmt.
    def visitScolon_stmt(self, ctx:XPPLL.Scolon_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#program.
    def visitProgram(self, ctx:XPPLL.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#as2.
    def visitAs2(self, ctx:XPPLL.As2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by XPPLL#asem2.
    def visitAsem2(self, ctx:XPPLL.Asem2Context):
        return self.visitChildren(ctx)



del XPPLL