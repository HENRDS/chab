# chab
Chab is a compiler for x++ writen in Python 3 and using ANTLR, the grammar is based on the one described by [Delamaro(2004)](https://sites.google.com/site/2012pcs25086482782/home/a-linguagem-x/gramatica-da-linguagem).

# Requirements

- Python (>=3.7);
- antlr4-python3-runtime(>=4.7.2)

You can also  install [Graphviz](http://graphviz.org) to generate images from the parsing trees. 

To install all dependencies run:

`python3.7 -m pip install -r "requirements.txt"`

# Quickstart
To execute chab there are 3 make files, one for each part of the project.

- [ASem1](ASem2.mk) reads from the input a series of statements, a semicolon and a variable declaration, then builds and prints the syntax tree for it;
- [GCI2](GCI2.mk) reads from the input a series of statements, then builds and prints the [Three address code](https://en.wikipedia.org/wiki/Three-address_code) generated for it;

To change the target file on ASem2 run:

`make -f ASem2.mk INPUT_FILE=path/to/file.xpp`


# The name
The name "chab" is an anagram with the initials of its creators:

**C**lailton\
**H**enry\
**A**ugusto\
**B**runo


