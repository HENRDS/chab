parser grammar XPPLL;


options { tokenVocab=XPPLexer; }


expr : equality (op=(LESS | LESS_EQUAL | GREATER | GREATER_EQUAL) expr)?
     ;

equality : lhs=add (op=(EQUAL_EQUAL|BANG_EQUAL) rhs=equality)?
         ;

add : lhs=mul (op=(PLUS | MINUS) rhs=add)?
    ;

mul : lhs=unary (op=(STAR | SLASH | PERCENT) rhs=mul)?
    ;

unary : primary
      | op=(PLUS|MINUS) rhs=primary
      ;

primary : access
        | factor
        | NEW alloc
        ;

factor : (INT_L | STR_L | NULL) #lit
       | LPARENS add RPARENS   #group
       ;


array_alloc: LBRACKET INT_L RBRACKET array_alloc?
           ;

alloc : access
      | typ=STRING array_alloc
      | typ=INT array_alloc
      ;

access : ID access_aux?
       ;

access_aux : LPARENS (args+=add (COMMA args+=add)*)? RPARENS access_aux?    #call_access
           | LBRACKET add RBRACKET access_aux?                              #get_item
           | DOT ID access_aux?                                             #get_attr
           ;

// Used by param, variable and method return type declarations
type_name : ID
          | STRING
          | INT
          ;


/* Variable declaration */
var_decl:  VAR type_=type_name variables+=name_decl (COMMA variables+=name_decl)* SEMICOLON
        ;

dec : VAR type_=type_name variables+=name_decl (COMMA variables+=name_decl)* ;

name_decl : name=ID dims+=dimension*
          ;

dimension : LBRACKET INT_L RBRACKET
               ;


/* Method, Constructor & Param */
meth_body : LPARENS (params+=param (COMMA params+=param)*)? RPARENS body=stmt
          ;

meth_decl: ret_type=type_name name=ID body=meth_body
         ;

param: type_=type_name  name=ID
     ;

ctor_decl : CTOR body=meth_body
          ;

/* Class */
cls_decl : CLASS name=ID (EXTENDS super_class=ID)? LBRACE
            nested+=cls_decl*
            variables+=var_decl*
            ctors+=ctor_decl*
            meths+=meth_decl*
           RBRACE
         ;

assign : lhs=access EQUAL rhs=expr
       ;



stmt : var_decl                             #decl
     | IF LPARENS condition=add RPARENS
          then_branch=stmt
       (ELSE else_branch=stmt)?             #if_stmt

     | FOR LPARENS assignment=assign?
           SEMICOLON condition=add?
           SEMICOLON step=assign?
           RPARENS body=stmt                #for_stmt
     | RETURN val=add? SEMICOLON           #return_stmt
     | BREAK SEMICOLON                      #break_stmt
     | PRINT val=add SEMICOLON             #print_stmt
     | READ des=access SEMICOLON            #read_stmt
     | LBRACE stmts+=stmt* RBRACE           #block_stmt
     | SUPER LPARENS args+=add* RPARENS SEMICOLON   #super_stmt
     | assign SEMICOLON                     #assign_stmt
     | SEMICOLON                            #scolon_stmt
     ;

/* Initital rule */
program : classes+=cls_decl* EOF
        ;

as2 : add SEMICOLON dec EOF;

asem2 : stmt+ EOF;