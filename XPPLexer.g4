lexer grammar XPPLexer;


fragment BIN_DIG : '0' | '1' ;
fragment OCT_DIG : BIN_DIG | '2' .. '7' ;
fragment DEC_DIG : OCT_DIG | '8' | '9' ;
fragment HEX_DIG : DEC_DIG | 'A'..'Z' | 'a'..'z' ;

fragment BIN_L :  BIN_DIG ('_'? BIN_DIG)* ;
fragment OCT_L :  OCT_DIG ('_'? OCT_DIG)* ;
fragment DEC_L :  DEC_DIG ('_'? DEC_DIG)* ;
fragment HEX_L :  HEX_DIG ('_'? HEX_DIG)* ;

INT_L : '0'( [bB] BIN_L |  [xX] HEX_L | [oO] OCT_L) | DEC_L ;
STR_L : '"' (~["\\] | '\\"' | '\\' ESC_SEQ )* '"' ;

fragment
ESC_SEQ : [\\abfnrtv0]
        | 'x' HEX_DIG HEX_DIG
        | 'o' OCT_DIG OCT_DIG OCT_DIG
        | 'u' HEX_DIG HEX_DIG HEX_DIG HEX_DIG
        | 'U' HEX_DIG HEX_DIG HEX_DIG HEX_DIG HEX_DIG HEX_DIG HEX_DIG HEX_DIG
        ;
WS : [ \n\t\r]+ -> skip ;

BANG_EQUAL : '!=' ;
BREAK: 'break' ;
CLASS : 'class' ;
COMMA : ',' ;
CTOR : 'constructor';
DOT : '.' ;
ELSE : 'else' ;
EQUAL : '=' ;
EQUAL_EQUAL : '==' ;
EXTENDS : 'extends' ;
FOR : 'for' ;
GREATER : '>' ;
GREATER_EQUAL : '>=' ;
IF : 'if' ;
INT : 'int';
LBRACE : '{' ;
LBRACKET : '[' ;
LESS : '<' ;
LESS_EQUAL : '<=' ;
LPARENS : '(' ;
MINUS : '-' ;
NEW : 'new' ;
NULL : 'null' ;
PERCENT : '%' ;
PLUS : '+' ;
PRINT : 'print' ;
READ : 'read' ;
RETURN: 'return' ;
SUPER: 'super' ;
STRING : 'string' ;
RBRACE : '}' ;
RBRACKET : ']' ;
RPARENS : ')';
SEMICOLON : ';' ;
SLASH : '/' ;
STAR : '*' ;
VAR : 'var' ;

fragment
NameChar
   : NameStartChar
   | '0'..'9'
   | '_'
   | '\u00B7'
   | '\u0300'..'\u036F'
   | '\u203F'..'\u2040'
   ;
fragment
NameStartChar
   : 'A'..'Z' | 'a'..'z'
   | '\u00C0'..'\u00D6'
   | '\u00D8'..'\u00F6'
   | '\u00F8'..'\u02FF'
   | '\u0370'..'\u037D'
   | '\u037F'..'\u1FFF'
   | '\u200C'..'\u200D'
   | '\u2070'..'\u218F'
   | '\u2C00'..'\u2FEF'
   | '\u3001'..'\uD7FF'
   | '\uF900'..'\uFDCF'
   | '\uFDF0'..'\uFFFD'
   ;

ID : NameStartChar NameChar*;

