from xpp import Visitor
from xpp.XPPLL import XPPLL
from functools import reduce
from operator import mul
import typing as tp


class ThreeAddressGenerator(Visitor):
    def __init__(self):
        self.__count_temp = 0
        self.__count_label = 0
        self.__is_new = False
        self.lines: tp.List[str] = []
        self.variables = {}
        self.type_sz = 4
        self.loop_out = ""

    def new_temp(self, sub_expr):
        key = f"T{self.__count_temp}"
        self.__count_temp += 1
        rhs = "".join(sub_expr)
        self.lines.append(f"{key} := {rhs}")
        return key

    def new_label(self):
        label = f"L{self.__count_label}"
        self.__count_label += 1
        return label

    def generate(self, tree):
        self.visit(tree)
        self.fix_stuff()
        return self.lines

    def generate_binary(self, ctx):
        lhs = self.visit(ctx.lhs)
        if ctx.op is None:
            return lhs

        if isinstance(lhs, tuple):
            lhs = self.new_temp(lhs)
        rhs = self.visit(ctx.rhs)
        if isinstance(rhs, tuple):
            rhs = self.new_temp(rhs)
        return lhs, ctx.op.text.join("  "), rhs

    def visitUnary(self, ctx: XPPLL.UnaryContext):
        if ctx.op is None:
            return self.visit(ctx.primary())
        rhs = self.visit(ctx.rhs)
        if isinstance(rhs, tuple):
            rhs = self.new_temp(rhs)
        return "0", ctx.op.text.join("  "), rhs

    def visitGroup(self, ctx: XPPLL.GroupContext):
        sub = self.visit(ctx.add())
        if isinstance(sub, tuple):
            sub = self.new_temp(sub)
        return sub

    def visitPrimary(self, ctx: XPPLL.PrimaryContext):
        if ctx.factor() is not None:
            return self.visit(ctx.factor())
        elif ctx.access() is not None:
            return self.visit(ctx.access())
        else:
            return self.visit(ctx.alloc())

    def visitAdd(self, ctx: XPPLL.AddContext):
        return self.generate_binary(ctx)

    def visitMul(self, ctx: XPPLL.MulContext):
        return self.generate_binary(ctx)

    def visitGet_item(self, ctx: XPPLL.Get_itemContext):
        item = self.visit(ctx.add())
        if isinstance(item, tuple):
            item = self.new_temp(item)
        result = ctx.lhs, "[", item, "]"
        if ctx.access_aux() is None:
            return result
        ctx.access_aux().lhs = self.new_temp(result)
        return self.visit(ctx.access_aux())

    def visitLit(self, ctx: XPPLL.LitContext):
        return ctx.getText()

    def visitAccess(self, ctx: XPPLL.AccessContext):
        name = ctx.ID().getText()
        aux = ctx.access_aux()
        if aux is None:
            return name
        aux.lhs = name
        return self.visit(aux)

    def visitArray_alloc(self, ctx: XPPLL.Array_allocContext):
        return super().visitArray_alloc(ctx)

    def visitCall_access(self, ctx: XPPLL.Call_accessContext):
        result = [ctx.lhs, "("]
        for arg in map(self.visit, ctx.args):
            result.append(self.new_temp(arg) if isinstance(arg, tuple) else arg)
            result.append(", ")
        if ctx.args:
            result.pop()
        result.append(")")
        result = tuple(result)
        if ctx.access_aux() is None:
            return result
        ctx.access_aux().lhs = self.new_temp(result)
        return self.visit(ctx.access_aux())

    def visitBreak_stmt(self, ctx: XPPLL.Break_stmtContext):
        self.lines.append(f"goto {self.loop_out}")

    def visitGet_attr(self, ctx: XPPLL.Get_attrContext):
        result = ctx.lhs, ".", ctx.ID().getText()
        if ctx.access_aux() is None:
            return result
        ctx.access_aux().lhs = self.new_temp(result)
        return self.visit(ctx.access_aux())

    def visitDimension(self, ctx: XPPLL.DimensionContext):
        return int(ctx.INT_L().getText())

    def visitName_decl(self, ctx: XPPLL.Name_declContext):
        dims = [self.type_sz] + [self.visit(d) for d in ctx.dims]
        size = reduce(mul, dims, 1)
        self.lines.append(f"{ctx.name.text} := alloc {size}")

    def visitDec(self, ctx: XPPLL.DecContext):
        for var in ctx.variables:
            self.visit(var)

    def visitFor_stmt(self, ctx: XPPLL.For_stmtContext):
        self.visit(ctx.assignment)
        loop = self.new_label()
        after = self.new_label()
        out = self.loop_out
        self.loop_out = after
        try:
            self.lines.append("")
            self.lines.append(f"{loop}:")
            cond = self.visit(ctx.condition)
            if isinstance(cond, tuple):
                cond = "".join(cond)
            self.lines.append(f"if not {cond} then goto {after}")
            self.visit(ctx.body)
            self.visit(ctx.step)
            self.lines.append(f"goto {loop}")
            self.lines.append("")
            self.lines.append(f"{after}:")
        finally:
            self.loop_out = out

    def visitIf_stmt(self, ctx: XPPLL.If_stmtContext):
        ans = self.visit(ctx.condition)
        if isinstance(ans, tuple):
            ans = self.new_temp(ans)
        lbl_then = self.new_label()
        lbl_after = self.new_label()
        self.lines.append(f"if {ans} then goto {lbl_then}")
        if ctx.else_branch:
            lbl_else = self.new_label()
            self.lines.append(f"goto {lbl_else}")
        else:
            self.lines.append(f"goto {lbl_after}")

        self.lines.append("")
        self.lines.append(f"{lbl_then}:")
        self.visit(ctx.then_branch)
        self.lines.append(f"goto {lbl_after}")
        self.lines.append("")
        if ctx.else_branch:
            self.lines.append(f"{lbl_else}:")
            self.visit(ctx.else_branch)
            self.lines.append(f"goto {lbl_after}")

        self.lines.append("")
        self.lines.append(f"{lbl_after}:")

    def visitReturn_stmt(self, ctx: XPPLL.Return_stmtContext):
        self.lines.append("goto QUIT")

    def visitRead_stmt(self, ctx: XPPLL.Read_stmtContext):
        self.lines.append(f"in {self.visit(ctx.des)}")

    def visitPrint_stmt(self, ctx: XPPLL.Print_stmtContext):
        ans = self.visit(ctx.val)
        if isinstance(ans, tuple):
            ans = "".join(ans)
        self.lines.append(f"out {ans}")

    def visitAssign(self, ctx: XPPLL.AssignContext):
        rhs = self.visit(ctx.rhs)
        if isinstance(rhs, tuple):
            rhs = "".join(rhs)
        self.lines.append(f"{self.visit(ctx.lhs)} := {rhs}")

    def visitAs2(self, ctx: XPPLL.As2Context):
        ans = self.visit(ctx.add())
        if isinstance(ans, tuple):
            ans = "".join(ans)
        self.lines.append(f"ans := {ans}")
        self.visit(ctx.dec())

    def swap_label(self, label: str, dest: str):
        lines = []
        for line in self.lines:
            if line == f"{label}:":
                continue
            lines.append(line.replace(label, dest))
        self.lines = lines

    def fix_stuff(self):
        if self.lines[-1].endswith(":"):
            self.swap_label(self.lines[-1].replace(":", ""), "QUIT")
        self.lines.append("QUIT:")
        x = False
        lines = []
        for line in self.lines:
            if x:
                if line.startswith("goto"):
                    continue
                else:
                    x = False
            else:
                x = line.startswith("goto")
            lines.append(line)
        self.lines = lines
