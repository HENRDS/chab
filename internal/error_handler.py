import typing as tp
from antlr4 import Token
from antlr4.error.ErrorListener import ErrorListener

_had_error = False


def had_error():
    return _had_error


def err_at_token(tk: Token, msg: str):
    text = f"\x1b[31mError: {msg} at {tk.line}:{tk.column}\x1b[0m"
    global _had_error
    _had_error = True
    print(text)


class ErrListener(ErrorListener):

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        global _had_error
        message = f"\x1b[31mError {msg} [{line}:{column}]\x1b[0m"
        print(message)
        _had_error = True

    def semanticError(self):
        pass


listener = ErrListener()


def unknown(tk: Token):
    err_at_token(tk, f"Unknown identifier '{tk.text}'")


def redeclared(tk: Token):
    err_at_token(tk, f"Redeclared identifier '{tk.text}'")
