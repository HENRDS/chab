from dataclasses import dataclass, field
import typing as tp
from textwrap import indent


@dataclass
class Declaration:
    name: str


@dataclass
class Type(Declaration):
    dimensions: int = 0

    def sub_dimension(self):
        if self.is_array:
            return Type(self.name, self.dimensions - 1)
        return None

    def upp_dimension(self):
        return Type(self.name, self.dimensions + 1)

    @property
    def is_array(self):
        return self.dimensions > 0

@dataclass
class Variable(Declaration):
    type: tp.Optional[Type]

    def __repr__(self):
        typ = self.type.name if self.type else "UNKNOWN"
        return f"var {self.name}: {typ}"
