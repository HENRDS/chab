import antlr4
import os
import typing as tp
from pathlib import Path
from xpp import Lexer, Parser
from internal.error_handler import listener, had_error
from internal.tree_printer import TreePrinter
from internal.three_addrs import ThreeAddressGenerator
from internal.type_checker import TypeChecker



class Chab:
    def lex_parse(self, stream: antlr4.InputStream):
        lexer = Lexer(stream)
        lexer._listeners = [listener]
        parser = Parser(antlr4.CommonTokenStream(lexer))
        parser._listeners = [listener]
        return lexer, parser

    def compile(self, stream):
        lexer, parser = self.lex_parse(stream)
        tree = parser.asem2()
        if had_error():
            print("Error during parsing.")
            return
        builder = TypeChecker()
        builder.visit(tree)
        if had_error():
            print("Error during semantic analysis.")
            return

        print("File parsed successfully.")


    def gen_code(self, stream):
        lexer, parser = self.lex_parse(stream)
        tree = parser.asem2()
        if had_error():
            print("Error during parsing.")
            return
        ir_gen = ThreeAddressGenerator()
        lines = ir_gen.generate(tree)
        print("\n".join(lines))

    def print_tree(self, stream):
        lexer, parser = self.lex_parse(stream)
        printer = TreePrinter()
        tree = parser.asem2()
        if had_error():
            print("Error during parsing.")
            return
        print(printer.print_tree(tree))

    def render_tree(self, stream, output: Path):
        lexer, parser = self.lex_parse(stream)
        printer = TreePrinter()
        tree = parser.as2()
        printer.render_tree(output, tree)

    def print_tokens(self, stream):
        lexer, parser = self.lex_parse(stream)
        tk: antlr4.Token = lexer.nextToken()
        while tk.type != antlr4.Token.EOF:
            print(f"<Token {Lexer.symbolicNames[tk.type]}: lexeme='{tk.text}'; pos={tk.line},{tk.column}>")
            tk = lexer.nextToken()
