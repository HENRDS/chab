from xpp import Visitor
from xpp.XPPLL import XPPLL
from contextlib import contextmanager
import typing as tp
from collections import ChainMap
from antlr4 import Token
from .error_handler import redeclared, unknown, err_at_token
from .model import Type, Variable


class TypeChecker(Visitor):
    def __init__(self):
        self.globl: ChainMap = ChainMap()
        self.current_scope: ChainMap = self.globl
        self.current_scope["int"] = Type("int")
        self.current_scope["string"] = Type("string")
        self.loop_depth = 0

    @contextmanager
    def new_scope(self, ctx: tp.Any):
        self.current_scope = self.current_scope.new_child()
        try:
            yield
        finally:
            ctx.xpp_scope = self.current_scope
            self.current_scope = self.current_scope.parents

    def resolve(self, symbol: Token):
        val = self.current_scope.get(symbol.text)
        if val is None:
            unknown(symbol)
        return val

    def visitVar_decl(self, ctx: XPPLL.Var_declContext):
        typ: Type = self.resolve(self.visit(ctx.type_))
        variables = []
        for var in ctx.variables:
            v: Variable = self.visit(var)
            if v is None:
                continue
            else:
                v.type = typ
            variables.append(v)
        ctx.xpp_obj = variables
        return variables

    def declare(self, name: Token, value):
        txt = name.text
        if txt in self.current_scope.maps[0]:
            redeclared(name)
            return False
        else:
            self.current_scope[txt] = value
            return True

    def visitName_decl(self, ctx: XPPLL.Name_declContext):
        name = ctx.name.text
        var = Variable(name, None)
        if ctx.dims:
            err_at_token(ctx.ID().getSymbol(), "Cannot define array variable")
        if self.declare(ctx.name, var):
            return var
        return None

    def visitBlock_stmt(self, ctx: XPPLL.Block_stmtContext):
        with self.new_scope(ctx):
            for stmt in ctx.stmts:
                self.visit(stmt)

    def visitAccess(self, ctx: XPPLL.AccessContext):
        x = self.resolve(ctx.ID().getSymbol())
        if x is None:
            return
        # em homenagem ao Isaías
        alce = ctx.access_aux()
        if alce is not None:
            self.visit(alce)
        return x.type

    def visitGet_attr(self, ctx: XPPLL.Get_attrContext):
        err_at_token(ctx.DOT().getSymbol(), "Cannot access attribute of variable")

    def visitCall_access(self, ctx: XPPLL.Call_accessContext):
        err_at_token(ctx.LPARENS().getSymbol(), "Cannot call variables")

    def visitGet_item(self, ctx: XPPLL.Get_itemContext):
        err_at_token(ctx.LBRACKET().getSymbol(), "variable has no subscribe operator!")

    def visitType_name(self, ctx: XPPLL.Type_nameContext):
        if ctx.ID() is not None:
            unknown(ctx.ID().getSymbol())
            return None
        elif ctx.STRING() is not None:
            return ctx.STRING().getSymbol()
        else:
            return ctx.INT().getSymbol()

    def check_bin_op(self, ctx):
        lhs_t = self.visit(ctx.lhs)
        if ctx.op is None:
            return lhs_t
        rhs_t = self.visit(ctx.rhs)
        if lhs_t is None or rhs_t is None:
            return None
        if lhs_t != rhs_t:
            err_at_token(ctx.op, f"Unsupported operand type(s) for {ctx.op.text}: '{lhs_t.name}' and '{rhs_t.name}'")
            return None
        return lhs_t

    def visitAdd(self, ctx: XPPLL.AddContext):
        return self.check_bin_op(ctx)

    def visitMul(self, ctx: XPPLL.MulContext):
        return self.check_bin_op(ctx)

    def visitUnary(self, ctx: XPPLL.UnaryContext):
        if ctx.op is None:
            return self.visit(ctx.primary())
        rhs_t = self.visit(ctx.rhs)
        if rhs_t.name != "int":
            err_at_token(ctx.op, f"Bad operand type for unary operator {ctx.op.text}: '{rhs_t.name}'")
            return None
        return rhs_t

    def visitPrimary(self, ctx: XPPLL.PrimaryContext):
        if ctx.NEW() is not None:
            err_at_token(ctx.NEW().getSymbol(), "Cannot instantiate undeclared class!")
        elif ctx.access() is not None:
            return self.visit(ctx.access())
        else:
            return self.visit(ctx.factor())

    def visitLit(self, ctx: XPPLL.LitContext):
        # The professor didn't specify what type null should be. We decided it is string!
        if ctx.INT_L() is not None:
            return self.globl["int"]
        else:
            return self.globl["string"]

    def visitGroup(self, ctx: XPPLL.GroupContext):
        return self.visit(ctx.add())

    def visitBreak_stmt(self, ctx: XPPLL.Break_stmtContext):
        if not self.loop_depth:
            err_at_token(ctx.BREAK().getSymbol(), "Cannot use break outside loop")
        return super().visitBreak_stmt(ctx)

    def visitFor_stmt(self, ctx: XPPLL.For_stmtContext):
        self.loop_depth += 1
        try:
            return super().visitFor_stmt(ctx)
        finally:
            self.loop_depth -= 1
