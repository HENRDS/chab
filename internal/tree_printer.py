from xpp import Visitor
from xpp.XPPLL import XPPLL
from textwrap import indent
from pathlib import Path


class Node:
    _ITEM_MARK = "-"

    def __init__(self, name, *subs):
        self.name = name
        self.children = [*subs]

    def add_child(self, sub):
        self.children.append(sub)

    def __str__(self):
        text = f" {self._ITEM_MARK} {self.name}"
        if self.children:
            text += "\n" + indent("\n".join(map(str, self.children)), "  ")
        return text


class TreePrinter(Visitor):
    def __init__(self):
        self.is_new = False

    def print_binary(self, name: str, ctx):
        node = Node(name)
        node.add_child(self.visit(ctx.lhs))
        aux = Node(f"{name}_aux")
        node.add_child(aux)
        if ctx.op is None:
            aux.add_child(Node("EPSILON"))
            return node
        aux.add_child(Node(ctx.op.text))
        aux.add_child(self.visit(ctx.rhs))
        return node

    def visitAdd(self, ctx: XPPLL.AddContext):
        return self.print_binary("add", ctx)

    def visitMul(self, ctx: XPPLL.MulContext):
        return self.print_binary("mul", ctx)

    def visitUnary(self, ctx: XPPLL.UnaryContext):
        node = Node("unary")
        if ctx.op is None:
            node.add_child(self.visit(ctx.primary()))
        else:
            node.add_child(Node(ctx.op.text))
            node.add_child(self.visit(ctx.rhs))
        return node

    def visitPrimary(self, ctx: XPPLL.PrimaryContext):
        node = Node("primary")
        if ctx.access() is not None:
            node.add_child(self.visit(ctx.access()))
        elif ctx.alloc() is not None:
            node.add_child(self.visit(ctx.alloc()))
        else:
            node.add_child(self.visit(ctx.factor()))
        return node

    def visitGroup(self, ctx: XPPLL.GroupContext):
        node = Node("grouping")
        node.add_child(self.visit(ctx.add()))
        return node

    def visitDec(self, ctx: XPPLL.DecContext):
        node = Node("var_decl", self.visit(ctx.type_))
        fst, *rest = ctx.variables
        node.add_child(self.visit(fst))
        parent = node
        for sub in rest:
            current = Node("var_names", self.visit(sub))
            parent.add_child(current)
            parent = current
        else:
            parent.add_child(Node("EPSILON"))
        return node

    def visitName_decl(self, ctx: XPPLL.Name_declContext):
        node = Node("name_decl", Node("ID", Node(ctx.ID().getText())))
        parent = Node("var_names")
        node.add_child(parent)
        for dim in ctx.dims:
            parent.add_child(self.visit(dim))
            cur = Node("var_names", )
            parent.add_child(cur)
            parent = cur
        else:
            parent.add_child(Node("EPSILON"))
        return node

    def visitDimension(self, ctx: XPPLL.DimensionContext):
        return Node("brackets", Node("INT_L", Node(ctx.INT_L().getText())))

    def visitType_name(self, ctx: XPPLL.Type_nameContext):
        return Node("type_name", Node(ctx.getText()))

    def visitLit(self, ctx: XPPLL.LitContext):
        if ctx.INT_L() is None:
            raise RuntimeError("Cannot print tree with this literal")
        return Node(ctx.INT_L().getText())

    def visitAccess(self, ctx: XPPLL.AccessContext):
        node = Node("access")
        node.add_child(Node(ctx.ID().getText()))
        if ctx.access_aux() is not None:
            node.add_child(self.visit(ctx.access_aux()))
        elif self.is_new:
            raise RuntimeError("Cannot instantiate attribute")
        return node

    def visitCall_access(self, ctx: XPPLL.Call_accessContext):
        node = Node("access_aux")
        for arg in ctx.args:
            node.add_child(self.visit(arg))

        if ctx.access_aux() is not None:
            node.add_child(self.visit(ctx.access_aux()))
        return node

    def visitGet_item(self, ctx: XPPLL.Get_itemContext):
        node = Node("access_aux")
        node.add_child(self.visit(ctx.add()))
        if ctx.access_aux() is not None:
            node.add_child(self.visit(ctx.access_aux()))
        return node

    def visitGet_attr(self, ctx: XPPLL.Get_attrContext):
        node = Node("access_aux", Node("access", Node(ctx.ID().getText()), Node("access_aux", Node("EPSILON"))))
        if ctx.access_aux() is not None:
            node.add_child(self.visit(ctx.access_aux()))
        return node

    def visitAlloc(self, ctx: XPPLL.AllocContext):
        node = Node("alloc")
        if ctx.access() is not None:
            self.is_new = True
            try:
                node.add_child(self.visit(ctx.access()))
            finally:
                self.is_new = False
        else:
            node.add_child(ctx.typ.text)
            node.add_child(self.visit(ctx.array_alloc()))
        return node

    def visitArray_alloc(self, ctx: XPPLL.Array_allocContext):
        node = Node("array_dims")
        node.add_child(Node("INT_L", Node(ctx.INT_L().getText())))
        if ctx.array_alloc() is not None:
            node.add_child(self.visit(ctx.array_alloc()))
        return node

    def print_tree(self, tree):
        return str(self.visit(tree))

    def visitAs2(self, ctx: XPPLL.As2Context):
        return Node("As2", self.visit(ctx.add()), Node(";"), self.visit(ctx.dec()), Node("EOF"))

    def render_tree(self, path: Path, tree):
        try:
            from graphviz import Graph
        except ImportError:
            print("Graphviz not installed, install if you want to render the trees")
            return
        nodes = self.visit(tree)
        graph = Graph(path.stem, format=path.suffix[1:])
        seen, pending = set(), {nodes}
        while pending:
            current = pending.pop()
            cur_id = str(hash(current))
            seen.add(current)
            attrs = {} if current.children else {"shape": "plaintext"}
            name = "\u03b5" if current.name == "EPSILON" else current.name
            graph.node(cur_id, name, attrs)
            for child in current.children:
                pending.add(child)
                graph.edge(cur_id, str(hash(child)))
            pending -= seen
        if not path.exists():
            path.touch()
        graph.render(str(path.with_suffix("")))
