#!/usr/bin/env python3
import sys
from internal.compiler import Chab
import argparse
import antlr4
from pathlib import Path

INTRO = """\
                     C.H.A.B. - A x++ compiler.
                 Copyright (c) 2019 (MIT) Augusto Zwirtes, 
             Bruno Manica, Clailton Francisco and Henry R. da Silva.
"""


def main():
    v = sys.version_info
    if not (v.major >= 3 and v.minor >= 7):
        raise ValueError("This version of python is not suitable for this project. Minimum required version is 3.7")
    parser = argparse.ArgumentParser("chab", "python3 main.py")
    parser.add_argument("file", help="Input file")
    options = parser.add_mutually_exclusive_group()
    options.add_argument("-k", "--tokens", action="store_true", help="Print tokens of the specified file.")
    options.add_argument("-s", "--string-tree", action="store_true", help="Print parsing tree to output.")
    options.add_argument("-g", "--gen-code", action="store_true",
                         help="Generate intermediate code for the string passed as the parameter.")
    args = parser.parse_args()
    print(INTRO)
    c = Chab()
    file = antlr4.FileStream(args.file, encoding="utf-8")
    if args.tokens:
        c.print_tokens(file)
    elif args.string_tree:
        c.print_tree(file)
    elif args.gen_code:
        c.gen_code(file)
    else:
        c.compile(file)




if __name__ == '__main__':
    main()
