import tarfile
from pathlib import Path
import zipfile
import os

excluded_files = {"venv", "dist", ".idea", ".tmp", ".git", "__pycache__"}
excluded_extensions = {".png", ".jpg", ".jpeg", ".pyc"}


def file_filter(info: tarfile.TarInfo):
    path = Path(info.name)
    if path.name in excluded_files:
        return None
    elif path.suffix in excluded_extensions:
        return None
    return info


def main():
    root_d = Path("../").resolve()
    chab_f = Path("../dist/chab.tar.gz").resolve()
    
    print(f"deploying {root_d} to {chab_f}")
    with zipfile.ZipFile(chab_f, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zfile:
        zfile.write()
    # with tarfile.open(chab_f, mode="w:gz") as seed_file:
    #     seed_file.add(root_d, arcname=root_d.name, filter=file_filter)


if __name__ == '__main__':
    main()
