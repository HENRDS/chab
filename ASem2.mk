#!/usr/bin/env make -f
include common.mk
INPUT_FILE="./tests/test1.xpp"


all :
	@$(PYTHON) main.py $(INPUT_FILE)

.PHONY : all
