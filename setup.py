from setuptools import setup


setup(
        name='chab',
        version='1.0',
        packages=['xpp', 'llvm', 'internal'],
        url='',
        license='MIT',
        author='Augusto G. L. Zwirtes;Buno Manica;Clailton Francisco;Henry R. Da Silva',
        author_email='',
        description='',
)
